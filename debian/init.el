;; IRC Server
;(setq liece-server '(:host "irc.kyoto.wide.ad.jp" :service 6667))

;; Private information of user.
;(setq liece-name "Your Name")
;(setq liece-nickname "nick")

;; Customization to change Look & Feel.
;; If non-nil, channel buffer would be displayed.
(setq liece-channel-buffer-mode t)
;; If non-nil, nick buffer would be displayed.
(setq liece-nick-buffer-mode t)
;; If non-nil, channel list buffer would be displayed.
(setq liece-channel-list-buffer-mode t)

(setq liece-intl-use-localized-message t)
(setq liece-window-default-style "top")
(setq liece-beep-on-bells 'always)
;(setq liece-beep-words-list '("foo" "bar"))
(setq liece-display-channel-always t)
(setq liece-display-time t)
(setq liece-display-prefix-tag t)
(setq liece-use-x-face t)
(setq liece-ctcp t)
(setq liece-insert-environment-version t)
(setq liece-beep-when-invited t)
(setq liece-beep-when-privmsg t)
(setq liece-reconnect-automagic t)
(setq liece-display-unread-mark t)

;; Highlighten IRC buffers.
(setq liece-highlight-mode t)
;; If `liece-highlight-mode' is non-nil, strings which matches 
;; following regular expression would be emphasized by colouring.
;(setq liece-highlight-pattern (regexp-opt '("foo" "bar")))

;; Channels we want to join startup time.
;(setq liece-startup-channel-list
;      '("#foo" "#bar"))
;; Channel bindings to its numerical expression.
;; Each element of list are bound to n-th.
;; DCC external programs.
;; When this is not specified, we search `dcc' executable in exec-path.
(setq liece-dcc t)
;; Don't receive any files automatically.
(setq liece-dcc-receive-direct nil) 

;;; XEmacs specific features
;; Normal position of toolbar icons.
(setq liece-toolbar-position 'top) 
;; Display smiley mark.
(setq liece-use-smiley t)          

;;; URL browsing.
;; Specify browser name. To see available browser names, 
;; refer docstring of `liece-url-browser-function'.
;(setq liece-url-browser-name "w3m") 

;; Automatic invisible.
(add-hook 'liece-after-001-hook
       	  (function (lambda (prefix rest)
		      (liece-send
		       "MODE %s +i" liece-real-nickname)
		      nil)))

;;; Converting codings.
;; Detect coding automatically.
;(setq liece-detect-coding-system t)
;; Convert deprecated hankaku kana to zenkaku kana.
;(setq liece-convert-hankaku-katakana t)
