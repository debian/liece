;;; plum-support.el --- pirc support for plum
;; Copyright (C) 1999 Daiki Ueno <ueno@ueda.info.waseda.ac.jp>

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1999-05-06
;; Keywords: IRC, liece, pirc

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; To use, add the following into your ~/.liece/init.el:
;; (eval-after-load "liece"
;;   '(progn
;;      (require 'plum-support)
;;      (add-hook 'liece-notice-hook 'plum-recent-add)
;;      (add-hook 'liece-002-hook 
;; 	       #'(lambda (header rest)
;; 		   (remove-hook 'liece-notice-hook 'plum-recent-add)))))

;;; Code:

(require 'custom)
(require 'liece-message)
(require 'liece-channel)

(defcustom plum-recent-header "plum"
  "String added in front of each recent logged message"
  :type 'string
  :group 'liece-support)

(defconst plum-recent-time-header-regexp "[0-9][0-9]:[0-9][0-9]"
  "Regexp for timestamp preceding in message body")

(defconst plum-recent-privmsg-header-regexp
  "[<>(=]\\([^ <>)=]*\\)[<>)=]"
  "Regexp for whole message line (privmsg)")

(defconst plum-recent-join-header-regexp
  "\\+ [@+]?[^ ]* [^ ]* to \\([^ ]*\\)"
  "Regexp for whole message line (join)")

(defconst plum-recent-part-header-regexp
  "- [^ ]* from \\([^ ]*\\)"
  "Regexp for whole message line (part)")

(defconst plum-recent-kick-header-regexp
  "- [^ ]* by [^ ]* from \\([^ ]*\\) .*"
  "Regexp for whole message line (kick)")

(defconst plum-recent-mode-header-regexp
  "Mode by [^:]*: \\([^ ]*\\) .*"
  "Regexp for whole message line (mode)")

(defconst plum-recent-topic-header-regexp
  "Topic of channel \\([^ ]*\\) by [^:]*: .*"
  "Regexp for whole message line (nick)")

(defconst plum-recent-message-header-regexp
  (concat "^\\(" plum-recent-time-header-regexp "\\)? *"
	  plum-recent-privmsg-header-regexp))

(defconst plum-recent-channel-header-regexp
  (concat "^\\(" plum-recent-time-header-regexp "\\)? *"
	  (mapconcat (function identity) 
		     (list plum-recent-join-header-regexp
			   plum-recent-part-header-regexp
			   plum-recent-kick-header-regexp
			   plum-recent-mode-header-regexp
			   plum-recent-topic-header-regexp)
		     "\\|")))

(defconst plum-recent-generic-header-regexp
  (concat "^\\(" plum-recent-time-header-regexp "\\)? *"))

(defvar plum-recent-log-buffer " *plum recent log")

(defun plum-parse-recent-after ()
  "Parse text after point as recent log message, then returns components"
  (save-excursion
    (let (time lparen rparen chnl nick)
      (cond
       ((looking-at plum-recent-message-header-regexp)
	(setq time (match-string 1))
	(goto-char (match-beginning 2))
	(setq lparen (char-before))
	(goto-char (match-end 2))
	(setq rparen (char-after))
	(skip-chars-backward "^: ") 
	(skip-chars-forward "=")
	(setq nick (buffer-substring (point) (match-end 2)))
	(backward-char)
	(setq chnl (buffer-substring (match-beginning 2) (point)))
	(goto-char (match-end 0))
	(forward-char)
	(list 'message time lparen rparen chnl nick 
	      (buffer-substring (point) (line-end-position))))
       ((looking-at plum-recent-channel-header-regexp)
	(setq time (match-string 1)
	      chnl (match-string 2))
	(goto-char (match-end 1))
	(forward-char)
	(list 'channel time chnl 
	      (buffer-substring (point) (line-end-position))))
       ((looking-at plum-recent-generic-header-regexp)
	(setq time (match-string 1))
	(goto-char (match-end 0))
	(list 'generic time (buffer-substring (point)(line-end-position))))))))

(defun plum-recent-add (header rest)
  "Add recent log line into `plum-recent-log-buffer'."
  (when (and (not header)
	     rest (string-match "^[^ ]* +:\\(.*\\)" rest))
    (with-temp-buffer 
      (insert (match-string 1 rest) ?\n)
      (goto-char (point-min))
      (let ((component (plum-parse-recent-after)))
	(when (eq (pop component) 'message)
	  (let ((speaker (nth 4 component))
		(target
		 (if (eq (nth 1 component) ?=)
		     (liece-current-nickname)
		   (if (memq (nth 1 component) '(?< ?>))
		       (liece-channel-virtual (nth 3 component)))))
		(text (concat "(" plum-recent-header " " (car component) 
			      ") " (nth 5 component))))
	    (liece-display-message
	     (liece-make-message speaker target text 'notice))
	    t))))))

(provide 'plum-support)

;;; plum-support.el ends here
