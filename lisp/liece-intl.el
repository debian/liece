;;; liece-intl.el --- Localized messages for IRC.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-12-30
;; Revised: 1999-09-03
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile
  (require 'cl)
  (require 'liece-compat))

(require 'gettext)

(defgroup liece-intl nil
  "Convert messages to localized them"
  :group 'liece-vars)

(defcustom liece-intl-catalogue-directory nil
  "Normal position of PO files."
  :type 'directory
  :group 'liece-intl)

(defcustom liece-intl-use-localized-messages t
  "If non-nil display localized messages."
  :type 'boolean
  :group 'liece-intl)
  
(defvar liece-intl-message-alist nil)
(defvar liece-intl-default-mime-charset 'x-ctext)
(defvar liece-intl-mime-charset nil)
(defvar liece-intl-domain "liece")

;;;###liece-autoload
(defmacro liece-intl-get-msgstr (msgid)
  `(if liece-intl-use-localized-messages
       (dgettext liece-intl-domain ,msgid)
     ,msgid))

;;;###liece-autoload
(defalias '_ 'liece-intl-get-msgstr)

(defun liece-intl-load-catalogue ()
  (if (null liece-intl-catalogue-directory)
      (setq liece-intl-catalogue-directory
	    (liece-locate-path "locale")))
  (bind-text-domain liece-intl-domain liece-intl-catalogue-directory))

(provide 'liece-intl)

;;; liece-intl.el ends here
