PACKAGE	= liece
VERSION	= 2.0.0

EMACS ?= emacs
XEMACS ?= xemacs
FLAGS ?= -batch -q -no-site-file
TAR ?= tar
PREFIX ?= NONE
LISPDIR	?= NONE
PACKAGEDIR ?= NONE
VERSION_SPECIFIC_LISPDIR ?= NONE

GOMI = liece-setup.el *.elc

all: elc

elc: 
	$(EMACS) $(FLAGS) -l ./liece-make.el -f autoload-liece \
		$(PREFIX) $(LISPDIR)
	$(EMACS) $(FLAGS) -l ./liece-make.el -f compile-liece \
		$(PREFIX) $(LISPDIR)

install: elc
	$(EMACS) $(FLAGS) -l ./liece-make.el -f install-liece \
		$(PREFIX) $(LISPDIR)

package:
	$(XEMACS) $(FLAGS) -l ./liece-make.el -f autoload-liece \
		$(PREFIX) $(LISPDIR)
	$(XEMACS) $(FLAGS) -l ./liece-make.el -l advice -f compile-liece-package \
		$(PACKAGEDIR)

install-package: package
	$(XEMACS) $(FLAGS) -l ./liece-make.el -f install-liece-package \
		$(PACKAGEDIR)

clean::
	-rm -f $(GOMI)
