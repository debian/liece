;;; liece-message.el --- generate and display message line
;; Copyright (C) 1999-2003 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Keywords: message

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:

(eval-when-compile (require 'liece-misc))

(defgroup liece-message nil
  "Messages"
  :tag "Message"
  :prefix "liece-"
  :group 'liece)

(defcustom liece-message-make-open-bracket-function
  #'liece-message-make-open-bracket
  "Function which makes `open-bracket' string for each message."
  :type 'function
  :group 'liece-message)

(defcustom liece-message-make-close-bracket-function
  #'liece-message-make-close-bracket
  "Function which makes `close-bracket' string for each message."
  :type 'function
  :group 'liece-message)

(defcustom liece-message-make-name-function
  #'liece-message-make-name
  "Function which makes local identity for each message."
  :type 'function
  :group 'liece-message)

(defcustom liece-message-make-global-name-function
  #'liece-message-make-global-name
  "Function which makes global identity for each message."
  :type 'function
  :group 'liece-message)

(defun liece-message-make-open-bracket (message)
  "Makes `open-bracket' string for MESSAGE."
  (liece-message-make-bracket message t))

(defun liece-message-make-close-bracket (message)
  "Makes `close-bracket' string for MESSAGE."
  (liece-message-make-bracket message nil))

(defun liece-message-make-bracket (message open-p)
  (if (eq open-p (liece-message-own-p message))
      (if (eq (liece-message-type message) 'notice)
	  "-"
	(if (eq (liece-message-type message) 'action)
	    "]"
	  (if (liece-message-private-p message)
	      "="
	    (if (liece-message-external-p message)
		")"
	      ">"))))
    (if (eq (liece-message-type message) 'notice)
	"-"
      (if (eq (liece-message-type message) 'action)
	  "["
	(if (liece-message-private-p message)
	    "="
	  (if (liece-message-external-p message)
	      "("
	    "<"))))))

(defun liece-message-make-name (message)
  "Makes local identity for MESSAGE."
  (if (and (liece-message-private-p message)
	   (liece-message-own-p message))
      (liece-message-target message)
    (liece-message-speaker message)))

(defun liece-message-make-global-name (message)
  "Makes global identity for MESSAGE."
  (if (liece-message-private-p message)
      (if (liece-message-own-p message)
	  (liece-message-target message)
	(liece-message-speaker message))
    (concat (liece-message-target message) ":"
	    (liece-message-speaker message))))

(defun liece-message-buffers (message)
  "Returns list of buffers where MESSAGE should appear."
  (let* ((target (if (liece-nick-equal (liece-message-target message)
				       (liece-current-nickname))
		     (liece-message-speaker message)
		   (liece-message-target message)))
	 (buffer (liece-pick-buffer target)))
    (if (car buffer)
	buffer
      (when liece-auto-join-partner
	(liece-channel-prepare-partner target)
	(liece-pick-buffer target)))))

(defun liece-message-parent-buffers (message buffer)
  "Returns the parents of BUFFER where MESSAGE should appear.
Normally they are *Dialogue* and/or *Others*."
  (if (or (and (car buffer) (liece-frozen (car buffer)))
	  (and (eq liece-command-buffer-mode 'channel)
	       (liece-current-channel)
	       (not (liece-channel-equal
		     (liece-message-target message)
		     (liece-current-channel))))
	  (and (eq liece-command-buffer-mode 'chat)
	       (liece-current-chat-partner)
	       (not (liece-message-own-p message))
	       (or (not (liece-nick-equal (liece-message-speaker message)
					  (liece-current-chat-partner)))
		   (not (liece-nick-equal (liece-message-target message)
					  (liece-current-nickname))))))
      (append liece-D-buffer liece-O-buffer)
    liece-D-buffer))

(defun liece-display-message (message)
  "Display MESSAGE object."
  (let* ((open-bracket
	  (funcall liece-message-make-open-bracket-function message))
	 (close-bracket
	  (funcall liece-message-make-close-bracket-function message))
	 (name
	  (funcall liece-message-make-name-function message))
	 (global-name
	  (funcall liece-message-make-global-name-function message))
	 (buffers (liece-message-buffers message))
	 (parent-buffers (liece-message-parent-buffers message buffers)))
    (liece-insert buffers
		  (concat open-bracket name close-bracket
			  " " (liece-message-text message) "\n"))
    (liece-insert parent-buffers
		  (concat open-bracket global-name close-bracket
			  " " (liece-message-text message) "\n"))
    (run-hook-with-args 'liece-display-message-hook message)))

(defun liece-make-message (speaker target text &optional type own-p)
  "Makes an instance of message object.
Arguments are apropriate to the sender, the receiver, and text
content, respectively.
Optional 4th argument TYPE specifies the type of the message.
Currently possible values are `action' and `notice'.
Optional 5th argument is the flag to indicate that this message is not
from the network."
  (vector speaker target text type own-p))

(defun liece-message-speaker (message)
  "Returns the sender of MESSAGE."
  (aref message 0))

(defun liece-message-target (message)
  "Returns the receiver of MESSAGE."
  (aref message 1))

(defun liece-message-text (message)
  "Returns the text part of MESSAGE."
  (aref message 2))

(defun liece-message-type (message)
  "Returns the type of MESSAGE.
Currently possible values are `action' and `notice'."
  (aref message 3))

(defun liece-message-own-p (message)
  "Returns t if MESSAGE is not from the network."
  (aref message 4))

(defun liece-message-private-p (message)
  "Returns t if MESSAGE is a private message."
  (liece-nick-equal (liece-message-target message)
		    (liece-current-nickname)))

(defun liece-message-external-p (message)
  "Returns t if MESSAGE is from outside the channel."
  (not (liece-channel-member (liece-message-target message)
			     (liece-nick-get-joined-channels
			      (liece-message-speaker message)))))

(defun liece-own-channel-message (message &optional chnl type)
  "Display MESSAGE as you sent to CHNL."
  (liece-display-message
   (liece-make-message (liece-current-nickname)
		       (or chnl (liece-current-channel))
		       message type t)))

(defun liece-own-private-message (message &optional partner type)
  "Display MESSAGE as you sent to PARTNER."
  (liece-display-message
   (liece-make-message (liece-current-nickname)
		       (or partner (liece-current-chat-partner))
		       message type t)))

(provide 'liece-message)

;;; liece-message.el ends here
