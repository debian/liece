;;; liece-mail.el --- Message composing and sending utility.
;; Copyright (C) 1999 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-12-29
;; Keywords: IRC, liece, Gnus

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(require 'liece-version)

(defgroup liece-mail nil
  "Compose and send mail in IRC buffer"
  :group 'liece-vars)

(defcustom liece-mail-draft-buffer " *mail*"
  "Generic draft buffer."
  :group 'liece-mail
  :type 'string)

(defcustom liece-maintainer-address
  "liece@unixuser.org (Liece developers)"
  "The mail address of the Liece maintainers."
  :group 'liece-mail
  :type 'string)

(defun liece-command-mail-compose (nick)
  (interactive
   (let ((completion-ignore-case t))
     (list
      (liece-minibuffer-completing-read
       "To whom: " liece-nick-alist nil nil nil nil liece-current-chat-partner))))
  (let ((composefunc (get mail-user-agent 'composefunc)) (to nick)
	(user-agent (liece-version)) uah)
    (if (setq uah (liece-nick-get-user-at-host nick))
	(setq to (concat to " <" uah ">")))
    (if (fboundp composefunc)
	(funcall composefunc to nil (` (("User-Agent" . (, user-agent))))))))

(defun liece-command-submit-bug-report ()
  "Send a bug report to the Gnus maintainers."
  (interactive)
  (require 'reporter)
  
  (reporter-submit-bug-report
   liece-maintainer-address
   (liece-version)
   (let ((base (list 'window-system
		     'liece-highlight-mode
		     'liece-detect-coding-system)))
     base)))
  
(provide 'liece-mail)

;;; liece-mail.el ends here
