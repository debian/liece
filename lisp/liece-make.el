;;; liece-make.el --- Generic make procedures.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1999-03-02
;; Keywords: IRC, liece, APEL

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(require 'cl)

(defun install-just-print-p ()
  (let ((flag (getenv "MAKEFLAGS"))
	case-fold-search)
    (princ (format "%s\n" flag))
    (if flag
	(string-match "^\\(\\(--[^ ]+ \\)+-\\|[^ =-]\\)*n" flag))))

(defun config-liece ()
  (let (prefix exec-prefix lisp-dir version-specific-lisp-dir)
    (and (setq prefix (car command-line-args-left))
         (or (string-equal "NONE" prefix)
             (setq PREFIX prefix)))
    (setq command-line-args-left (cdr command-line-args-left))
    (and (setq lisp-dir (car command-line-args-left))
         (or (string-equal "NONE" lisp-dir)
             (setq LISPDIR lisp-dir)))
    (setq command-line-args-left (cdr command-line-args-left))
    (and (setq version-specific-lisp-dir (car command-line-args-left))
         (or (string-equal "NONE" version-specific-lisp-dir)
             (progn
               (defvar VERSION_SPECIFIC_LISPDIR version-specific-lisp-dir)
               (princ (format "VERSION_SPECIFIC_LISPDIR=%s\n"
                              VERSION_SPECIFIC_LISPDIR)))))
    (setq command-line-args-left (cdr command-line-args-left))
    (setq load-path (cons (expand-file-name ".") load-path))
    (load "liece-config")
    (or (boundp 'liece-modules-to-compile)
	(load "liece-modules"))
    (princ (format "PREFIX=%s\tLISPDIR=%s\n" PREFIX LISPDIR))))

(defun compile-liece ()
  ;;(setq byte-compile-dynamic t)
  (config-liece)
  (compile-elisp-modules liece-modules-to-compile "."))

(defun install-liece ()
  (compile-liece)
  (let ((just-print (install-just-print-p))
	(dir (expand-file-name "liece" LISPDIR)))
    (princ (format "%s\n" emacs-version))
    (install-elisp-modules liece-modules "." dir just-print)))

(defun install-update-manifest-file (package dir &optional just-print)
  (message "Generating MANIFEST.%s for the package..." package)
  (unless just-print
    (with-temp-buffer
      (insert "pkginfo/MANIFEST." package "\n"
	      "lisp/" package "/"
	      (mapconcat #'identity
			 (sort
			  (mapcar (lambda (symbol)
				    (format "%s.el\nlisp/%s/%s.elc"
					    symbol package symbol))
				  liece-modules-to-compile)
			  #'string-lessp)
			 (concat "\nlisp/" package "/"))
	      "\n")
      (when (file-directory-p "../doc")
	(insert "info/"
		(mapconcat #'identity
			   (sort
			    (directory-files
			     "../doc" nil liece-config-info-file-regexp)
			    #'string-lessp)
			   "\ninfo/")
		"\n"))
      (let ((dirs '("icons" "po" "styles")))
	(dolist (dir dirs)
	  (when (file-directory-p (concat "../etc/" dir))
	    (insert "etc/" package "/" dir "/"
		    (mapconcat #'identity
			       (sort (directory-files (concat "../etc/" dir)
						      nil nil nil t)
				     #'string-lessp)
			       (concat "\netc/" package "/" dir "/"))
		    "\n"))))
      (write-file (expand-file-name (concat "MANIFEST." package) dir)))))

(defun install-update-package-files (package dir &optional just-print)
  (cond (just-print
	 (princ (format "Updating autoloads in directory %s..\n\n" dir))
	   
	 (princ (format "Processing %s\n" dir))
	 (princ "Generating custom-load.el...\n\n")
	    
	 (princ (format "Compiling %s...\n"
			(expand-file-name "auto-autoloads.el" dir)))
	 (princ (format "Wrote %s\n"
			(expand-file-name "auto-autoloads.elc" dir)))
	   
	 (princ (format "Compiling %s...\n"
			(expand-file-name "custom-load.el" dir)))
	 (princ (format "Wrote %s\n"
			(expand-file-name "custom-load.elc" dir))))
	(t
	 (if (fboundp 'batch-update-directory-autoloads)
	     (progn
	       (setq command-line-args-left
		     (cons package (cons dir command-line-args-left)))
	       (batch-update-directory-autoloads))
	   (setq autoload-package-name package)
	   (add-to-list 'command-line-args-left dir)
	   (batch-update-directory))
	
	 (add-to-list 'command-line-args-left dir)
	 (Custom-make-dependencies)
	   
	 (byte-compile-file (expand-file-name "auto-autoloads.el" dir))
	 (byte-compile-file (expand-file-name "custom-load.el" dir)))))

(defun config-liece-package-directory ()
  (if (boundp 'early-packages)
      (let ((dirs (append (if early-package-load-path
			      early-packages)
			  (if late-package-load-path
			      late-packages)
			  (if last-package-load-path
			      last-packages)))
	    dir)
	(while (and dirs (not (file-exists-p
			       (setq dir (car dirs)))))
	  (setq dirs (cdr dirs)))
	(defvar PACKAGEDIR dir)
	(princ (format "PACKAGEDIR=%s\n" PACKAGEDIR)))))

(defun config-liece-package ()
  (let (package-dir)
    (and (setq package-dir (car command-line-args-left))
	 (or (string= "NONE" package-dir)
	     (defvar PACKAGEDIR package-dir)))
    (config-liece)
    (config-liece-package-directory)
    (setq command-line-args-left (cdr command-line-args-left))))

(defun compile-liece-package ()
  (config-liece-package)
  (compile-elisp-modules liece-modules-to-compile "."))

(defun install-liece-package ()
  (config-liece-package)
  (let ((just-print (install-just-print-p))
	(dir (expand-file-name "lisp/liece" PACKAGEDIR))
	(pkginfo-dir (expand-file-name "pkginfo" PACKAGEDIR)))
    (install-elisp-modules liece-modules "." dir just-print)
    (install-update-package-files "liece" dir just-print)
    (install-update-manifest-file "liece" pkginfo-dir just-print)))

(defun autoload-liece ()
  (config-liece)
  (require 'autoload)
  (let* ((generated-autoload-file "liece-setup.el")
	 (generate-autoload-cookie ";;;###liece-autoload")
	 (buf (find-file-noselect generated-autoload-file))
	 make-backup-files)
    (set-buffer buf)
    (delete-region (point-min) (point-max))
    (insert-string
     (format "(if (not (featurep '%s)) (progn\n"
	     (file-name-sans-extension generated-autoload-file)))
    (mapcar
     (function
      (lambda (file)
	(generate-file-autoloads
	 (concat (symbol-name file) ".el"))))
     liece-modules-to-compile)
    (goto-char (point-max))
    (insert-string
     (format "(provide '%s)))\n"
	     (file-name-sans-extension generated-autoload-file)))
    (save-buffer)))

(provide 'liece-make)

;;; liece-make.el ends here
