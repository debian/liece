;;; liece-q-el.el --- CTCP binary data quotation in emacs-lisp.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1999-01-31
;; Revised: 1999-01-31
;; Keywords: IRC, liece, CTCP

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(defconst liece-quote-strings
  '(("\\\\" . "\\") ("\\a" . "") ("\\n" . "\n") ("\\r" . "\r")))
  
(defun liece-quote-el-decode-region (start end)
  "Decode binary data between START and END."
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (dolist (pair liece-quote-strings)
      (goto-char (point-min)) (replace-string (car pair) (cdr pair)))))

(defun liece-quote-el-decode-string (string)
  "Decode binary data from STRING."
  (interactive)
  (save-excursion
    (with-temp-buffer
      (insert string)
      (liece-quote-el-decode-region (point-min) (point-max))
      (buffer-substring (point-min) (point-max)))))

(defun liece-quote-el-encode-region (start end)
  "Encode binary data between START and END."
  (interactive "r")
  (save-restriction
    (narrow-to-region start end)
    (goto-char (point-min))
    (while (search-forward "\\" nil t) (insert "\\"))
    (goto-char (point-min))
    (while (search-forward "" nil t)
      (backward-delete-char 1)
      (insert "\\a"))
    (goto-char (point-min))
    (while (search-forward "\n" nil t)
      (backward-delete-char 1)
      (insert "\\n"))
    (goto-char (point-min))
    (while (search-forward "\r" nil t)
      (backward-delete-char 1)
      (insert "\\r"))))

(defun liece-quote-el-encode-string (string)
  "Encode binary data from STRING."
  (interactive)
  (save-excursion
    (with-temp-buffer
      (erase-buffer)
      (insert string)
      (liece-quote-el-encode-region (point-min) (point-max))
      (buffer-substring (point-min) (point-max)))))

(defalias 'liece-quote-decode-string 'liece-quote-el-decode-string)
(defalias 'liece-quote-encode-string 'liece-quote-el-encode-string)

(defalias 'liece-quote-decode-region 'liece-quote-el-decode-region)
(defalias 'liece-quote-encode-region 'liece-quote-el-encode-region)

(provide 'liece-q-el)

;;; liece-q-el.el ends here

