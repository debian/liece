;;; liece-vars.el --- Customization.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile
  (require 'liece-compat)
  (require 'liece-globals))

;; User modifiable variables.
(defgroup liece nil
  "liece specific customize group")

(defgroup liece-vars nil
  "liece user customizable variables"
  :tag "Variables"
  :prefix "liece-"
  :group 'liece)

(defcustom liece-saved-forms
  '(liece-kill-nickname
    liece-server-alist
    liece-channel-buffer-mode
    liece-nick-buffer-mode
    liece-friends)
  "Variables whose values are saved via command liece-command-save-variables."
  :type 'string
  :group 'liece-vars)

(defgroup liece-look nil
  "Look & Feels"
  :tag "Look"
  :prefix "liece-"
  :group 'liece)

(defcustom liece-command-window-height 4
  "How large should command window be on startup."
  :type 'integer
  :group 'liece-look)

(defcustom liece-truncate-partial-width-windows nil
  "If non-nil, truncate lines in splitting windows such as others buffer."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-use-full-window t
  "If non-nil, IRCHAT will use whole Emacs window."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-display-frame-title t
  "If non-nil, IRCHAT displays channel information on frame title."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-display-unread-mark t
  "If non-nil, display unread mark as XEmacs glyphs or character to channel
list buffer."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-channel-unread-character "!"
  "Channel unread character."
  :type 'character
  :group 'liece-look)

(defcustom liece-tab-stop-list '(2 4 6 8 10 12 14 16)
  "List of tab stop positions in dialogue buffer."
  :type '(repeat integer)
  :group 'liece-look)

(defcustom liece-icon-directory nil
  "Normal position of liece icons."
  :type 'directory
  :group 'liece-look)

(defcustom liece-nick-image-alist
  '((?@ . "ball.red.xpm")
    (?  . "ball.blank.xpm")
    (?+ . "ball.gray.xpm"))
  "Normal and deformed faces for operators and voices."
  :type '(repeat (list (character :tag "Mark")
		       (string :tag "Image")))
  :group 'liece-look)

(defcustom liece-inhibit-startup-message nil
  "If non-nil, the startup message will not be displayed.
This variable is used before `.liece/init.el' is loaded, so it should
be set in `.emacs' instead."
  :group 'liece-look
  :type 'boolean)

(defcustom liece-display-status-on-channel-indicator nil
  "When non-nil, liece will display channel topic on modeline."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-directory "~/.liece"
  "Where to look for data files."
  :type 'directory
  :group 'liece-vars)
  
(defcustom liece-variables-file
  (expand-file-name "init" liece-directory)
  "Where to look for variables."
  :type 'file
  :group 'liece-vars)

(defcustom liece-saved-variables-file
  (expand-file-name "save" liece-directory)
  "Where to look for variables.
This file was saved the last session."
  :type 'file
  :group 'liece-vars)

(defcustom liece-variables-files
  (list liece-variables-file liece-saved-variables-file)
  "Where to look for variables.  Helps to remove clutter from your .emacs.
This feature is most likely to dissappear in near future.  The preferred
way is to put liece variables on .emacs or file loaded from there."
  :type '(repeat (file :tag "Initialization File"))
  :group 'liece-vars)

;; Where to connect.
(defgroup liece-server nil
  "Server Settings"
  :tag "Server"
  :prefix "liece-"
  :group 'liece)

(defgroup liece-channel nil
  "Channel Settings"
  :tag "Channel"
  :prefix "liece-"
  :group 'liece)

(define-widget 'liece-service-spec 'radio
  "Edit service spec entries"
  :convert-widget 'liece-service-spec-convert)

(defun liece-service-spec-convert (widget)
  (widget-put widget :args '((integer :tag "Port Number")
			     (string :tag "Name")))
  widget)

(define-widget 'liece-server-spec 'repeat
  "Edit server spec entries"
  :match (lambda (widget value)
	   (eval
	    (` (and
		(,@ (mapcar
		     (lambda (entry)
		       (or (stringp (cdr entry))
			   (listp (cdr entry))))
		     value))))))
  :convert-widget 'liece-server-spec-convert)

(defun liece-server-spec-convert (widget)
  (let* ((host '(const :format "" :value :host))
	 (service '(const :format "" :value :service))
	 (host
	  (` (group :inline t (, host) (string :tag "Host"))))
	 (service
	  (` (group :inline t (, service) liece-service-spec)))
	 (spec
	  (` (cons
	      (string :tag "Name")
	      (radio (string :tag "Host")
		     (list (, host) (, service))))))
	 (args (` ((, spec)))))
    (widget-put widget :args args)
    widget))
  
(defcustom liece-server-alist nil
  "IRC server assoc-list which is used for input IRC server."
  :type 'liece-server-spec
  :group 'liece-server)

(defcustom liece-server (or (getenv "IRCSERVER") nil)
  "IRC server host we are connecting to."
  :type 'string
  :group 'liece-server)

(defcustom liece-service
  (let ((ircport-env (getenv "IRCPORT")))
    (if ircport-env
	(if (> (string-to-int ircport-env) 0)
	    (string-to-int ircport-env)
	  ircport-env)
      6667))
  "IRC service name or (port) number."
  :type 'liece-service-spec
  :group 'liece-server)

(defcustom liece-password (or (getenv "IRCPASSWORD") nil)
  "Your password when connecting to server."
  :type '(radio (string :tag "Password")
		(const :tag "No" nil))
  :group 'liece-server)

(defcustom liece-name (or (getenv "IRCNAME") (user-real-login-name))
  "The username you want to use in IRC."
  :type 'string
  :group 'liece-server)

(defcustom liece-nickname (or (getenv "IRCNICK") (user-real-login-name))
  "The nickname you want to use in IRC."
  :type 'string
  :group 'liece-server)

(defcustom liece-startup-channel nil
  "The channel to join automatically at startup.
If nil, do not join any channel."
  :type '(radio (string :tag "Channels")
		(const nil))
  :group 'liece-channel)

(defcustom liece-startup-channel-list nil
  "The channel list to join automatically at startup.
If nil, do not join any channel."
  :type '(repeat (string :tag "Startup Channel"))
  :group 'liece-channel)

(defcustom liece-auto-join-partner t
  "Whether join automatically at arriving private message."
  :type 'boolean
  :group 'liece-channel)

(defcustom liece-reconnect-automagic nil
  "Automatic reconnection, default is disabled."
  :type 'boolean
  :group 'liece-server)

(defcustom liece-ask-for-nickname nil
  "Ask for nickname if liece was entered with \\[universal-argument]."
  :type 'boolean
  :group 'liece-server)

(defcustom liece-ask-for-password nil
  "Ask for password when connecting to server."
  :type 'boolean
  :group 'liece-server)

(defcustom liece-ask-for-channel-password nil
  "Ask for channel password when joining channel with password."
  :type 'boolean
  :group 'liece-server)

(defcustom liece-reconnect-with-password nil
  "Auto recconect to server with password after incorrect password."
  :type 'boolean
  :group 'liece-server)

(defcustom liece-retry-with-new-nickname nil
  "When nickname has already been in use, grow-tail automagically."
  :type 'boolean
  :group 'liece-server)

;; Hooks.
(defgroup liece-hook nil
  "Hooks"
  :tag "Hooks"
  :prefix "liece-"
  :group 'liece)
  
(defcustom liece-command-mode-hook nil
  "A hook for IRCHAT command mode."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-dialogue-mode-hook nil
  "A hook for IRCHAT dialogue mode."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-others-mode-hook nil
  "A hook for IRCHAT others mode."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-channel-mode-hook nil
  "A hook for IRCHAT Channel mode."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-nick-mode-hook nil
  "A hook for IRCHAT nick mode."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-exit-hook nil
  "A hook executed when signing off IRC."
  :type 'hook
  :group 'liece-hook)

(defcustom liece-before-kill-emacs-hook nil
  "A hook executed when exitting Emacs."
  :type 'hook
  :group 'liece-hook)

;; Channel buffers.
(defcustom liece-channel-buffer-mode t
  "When non-nil, liece will display a channel buffer."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-channel-window-height-percent 63
  "How large percent should Current channel window be on startup."
  :type 'integer
  :group 'liece-look)

(defcustom liece-default-beep nil
  "If non nil, channel buffer local beep flag is on at starting."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-default-freeze nil
  "If non nil, channel buffer local freeze flag is on at starting."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-default-own-freeze nil
  "If non nil, channel buffer local own freeze flag is on at starting."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-default-channel-binding nil
  "The channel list to bind the channel number when joining."
  :type '(repeat (radio (string :tag "Bound Channel")
			(const nil)))
  :group 'liece-channel)

(defcustom liece-default-partner-binding nil
  "The chat partner list to bind the partner's number."
  :type '(repeat (radio (string :tag "Bound Partner")
			(const nil)))
  :group 'liece-partner)

;; Nick buffers.
(defcustom liece-nick-buffer-mode t
  "When non-nil, liece will display a nick list buffer."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-nick-window-width-percent 18
  "How large percent should Current nick list window be on startup."
  :type 'integer
  :group 'liece-look)

(defcustom liece-nick-window-auto-hide t
  "Hide nick window in priv buffer automatically."
  :type 'boolean
  :group 'liece-look)

;; Channel list buffer.
(defcustom liece-channel-list-buffer-mode t
  "When non-nil, liece will display a channel list buffer."
  :type 'boolean
  :group 'liece-look)

(defcustom liece-channel-list-window-width-percent 18
  "How large percent should Current channel list window be on startup."
  :type 'integer
  :group 'liece-look)
  
;; Highlight.
(defcustom liece-highlight-mode t
  "If non nil, IRC buffer is highlighted by specific rule."
  :type 'boolean
  :group 'liece-highlight)

(defcustom liece-highlight-pattern "$^"
  "If non nil, this pattern used for user defined highlight rule."
  :type '(regexp :tag "Highlighting Pattern")
  :group 'liece-highlight)

;; DCC
(defgroup liece-dcc nil
  "DCC Settings"
  :tag "DCC"
  :prefix "liece-"
  :group 'liece)

(defcustom liece-dcc-program "ldcc"
  "Name of the external dcc-program.
Current version takes the one which is earlier in path \(if many\)."
  :type '(file :tag "DCC program path")
  :group 'liece-dcc)

(defcustom liece-dcc-directory "~/tmp"
  "Directory where liece-dcc puts its files."
  :type 'directory
  :group 'liece-dcc)

(defcustom liece-dcc-port 0
  "Default port for DCC operations."
  :type 'integer
  :group 'liece-dcc)

(defcustom liece-dcc-resolve-server nil
  "Resolve IRC server FQDN with external DCC command."
  :type 'boolean
  :group 'liece-dcc)

;; Prefix strings for various messages.
(defgroup liece-prefix nil
  "Various message prefix"
  :tag "Prefix"
  :prefix "liece-"
  :group 'liece)

(defgroup liece-prefix-string nil
  "Various prefix string"
  :tag "Prefix string"
  :prefix "liece-"
  :group 'liece-prefix)

(defcustom liece-change-prefix "*** Change: "
  "String to add before changing messages."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-notice-prefix "*** Notice: "
  "String to add before any notice message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-broadcast-prefix "*** Broadcast: "
  "String to add before any Broadcast message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-wallops-prefix "*** Notice: "
  "String to add before any WALLOPS message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-error-prefix "*** Error: "
  "String to add before any ERROR message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-info-prefix "*** Info: "
  "String to add before any informational message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-timestamp-prefix "*** Time: "
  "String to add before any timestamp message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-client-prefix "*** Client: "
  "String to add before any CTCP message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-dcc-prefix "*** DCC: "
  "String to add before any DCC message."
  :type 'string
  :group 'liece-prefix-string)

(defcustom liece-time-prefix-regexp "[0-9][0-9]:[0-9][0-9] "
  "Time prefix regexp."
  :type 'regexp
  :group 'liece-prefix)

(defcustom liece-display-time t
  "If non-nil, print time prefix on each line in channel buffer."
  :type 'boolean
  :group 'liece-prefix)

(defcustom liece-display-prefix-tag nil
  "If non-nil, print prefix tag on each line in channel buffer."
  :type 'boolean
  :group 'liece-prefix)

(defcustom liece-generic-prefix-tag-regexp "\\*\\*\\* \\([^:]+: \\)"
  "Regular expression for generic prefix tag."
  :type 'regexp
  :group 'liece-prefix)

;; Misc.
(defcustom liece-blink-parens nil
  "Should we blink matching parenthesis in liece command buffer?"
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-show-wallops t
  "Show wallops messages if usermode +ws."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-ignore-extra-notices t
  "Don't show any notice unless they come from the local server."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-shorten-kills t
  "Shorten KILL messages to about one line."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-ignore-changes nil
  "Ignore changes? Good in topic-wars/link troubles."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-ignore-fakes nil
  "If non nil, ignore fake notices if receiving them."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-signoff-message nil
  "Default signoff message."
  :type '(radio (string :tag "Signoff message"))
  :group 'liece-vars)

(defcustom liece-away-message nil
  "Default away message."
  :type 'string
  :group 'liece-vars)

(defcustom liece-beep-on-bells 'always
  "If non-nil, and the IRC dialogue buffer is not selected in a window,
an IRC message arriving containing a bell character, will cause you
to be notified.
If value is 'always, an arriving bell will always cause a beep (or flash)."
  :type '(radio (const :tag "Always" always) (const :tag "No" nil))
  :group 'liece-vars)

(defcustom liece-beep-function (function ding)
  "Function to beep."
  :type 'function
  :group 'liece-vars)

(defcustom liece-beep-when-invited nil
  "If non-nil, beep when invited."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-beep-when-privmsg nil
  "If non-nil, beep when received privmsg."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-beep-words-list nil
  "This list used for user defined beep rules."
  :type '(repeat (string :tag "Beep Rules List"))
  :group 'liece-vars)

(defcustom liece-system-fqdname (system-name)
  "The fully qualified domain name of the system.
Default is what (system-name) returns."
  :type 'string
  :group 'liece-vars)

(defcustom liece-gather-channel-modes t
  "Gather channel modes when joining channels."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-use-x-face nil
  "Display X-Face in dialogue buffer."
  :type 'boolean
  :group 'liece-look)

;; Send/Receive files.
(defcustom liece-file-accept nil
  "Do we accept files."
  :type 'boolean
  :group 'liece-ctcp)

(defcustom liece-file-confirm-save nil
  "Do we want confirmation on saving files."
  :type 'boolean
  :group 'liece-ctcp)

;; Userinfos.
(defgroup liece-ctcp nil
  "CTCP Settings"
  :tag "CTCP"
  :prefix "liece-"
  :group 'liece)

(defcustom liece-ctcp-userinfo "No user information given."
  "Userinfo message given to anyone asking."
  :type 'string
  :group 'liece-ctcp)

(defcustom liece-ctcp-x-face ""
  "X-Face message given to anyone asking.  Proper form is \"X-Face: ....\"."
  :type 'string
  :group 'liece-ctcp)

(static-when (featurep 'xemacs)
  (define-obsolete-variable-alias 'liece-client-userinfo
    'liece-ctcp-userinfo)
  (define-obsolete-variable-alias 'liece-client-x-face
    'liece-ctcp-x-face))

(defcustom liece-timers
  '((liece-command-poll-names liece-poll-names-interval)
    (liece-command-keepalive liece-keepalive-interval)
    (liece-command-poll-friends liece-poll-friends-interval))
  "Symbol name to store timer, timer-function and timer-interval."
  :type 'list
  :group 'liece-server)

(defcustom liece-friends nil
  "Friends to be checked consequently."
  :type '(repeat (string :tag "Nick"))
  :group 'liece-vars)

(defcustom liece-display-friends-function 'liece-303-display-friends
  "Function to display friends status."
  :type 'function
  :group 'liece-vars)

(defcustom liece-poll-friends-interval 30
  "Interval on seconds the existence of friends."
  :type '(repeat
	  (integer :tag "Polling Interval in Seconds")
	  (const :tag "No" nil))
  :group 'liece-server)

(defcustom liece-keepalive-interval nil
  "Interval on seconds the existence of server connection is checked."
  :type '(radio
	  (integer :tag "Seconds"
		   :doc "Connection Interval in Seconds")
	  (const :tag "No" nil))
  :group 'liece-server)

(defcustom liece-poll-names-interval nil
  "Interval the names are polled from server."
  :type '(radio
	  (integer :tag "Seconds"
		   :doc "NAMES Polling Interval in Seconds")
	  (const :tag "No" nil))
  :group 'liece-server)

(defcustom liece-timestamp-interval (* 60 10)
  "Interval in seconds between timestamps in dialogue-buffer, nil for none."
  :type '(radio
	  (integer :tag "Seconds"
		   :doc "Timestamp Interval in Seconds")
	  (const "No" nil))
  :group 'liece-server)

(defcustom liece-connection-timeout 60
  "Default timeout interval."
  :type 'integer
  :group 'liece-server)
  
(defcustom liece-timestamp-format (concat liece-timestamp-prefix "%s")
  "Format-string for timestamp."
  :type 'string
  :group 'liece-format-string)

;; Conversions: Not used.
(defcustom liece-send-convert-list nil
  "Convert characters before sending to server."
  :type '(repeat (character :tag "Character"
			    :doc "Convert Character"))
  :group 'liece-vars)

(defcustom liece-receive-convert-list nil
  "Convert characters after receiving from server."
  :type '(repeat (character :tag "Character"
			    :doc "Convert Character"))
  :group 'liece-vars)

;; Ignores
(defgroup liece-ignore nil
  "Ignore Settings"
  :tag "Ignore"
  :prefix "liece-"
  :group 'liece-vars)

(defcustom liece-kill-nickname nil
  "List of nicknames to be ignored.
Messages from these people won't be displayed."
  :type '(repeat (string :tag "Nick" :doc "Ignore Nick"))
  :group 'liece-ignore)

(defcustom liece-kill-realname nil
  "List of real names to be ignored.
Messages from them won't be displayed."
  :type '(repeat (string :tag "Realname" :doc "Ignore Realname"))
  :group 'liece-ignore)

(defcustom liece-kill-logon nil
  "List of logon names (user@host.dom.ain) to be ignored.
Messages from them won't be displayed."
  :type '(repeat (string :tag "Logon" :doc "Ignore Logon Name"))
  :group 'liece-ignore)

;; New style ignore.
(defcustom liece-no-ignore-buffers nil
  "A list of buffers that don't adhere to ignore-list."
  :type '(repeat (string :tag "Buffer" :doc "Don't Ignore Buffer Names"))
  :group 'liece-ignore)

(define-widget 'liece-ignore-spec 'repeat
  "Edit server spec entries"
  :convert-widget 'liece-ignore-spec-convert)

(defun liece-ignore-spec-convert (widget)
  (let* ((type
	  '(radio
	    (string :tag "Type" :doc "Match for messagetype")
	    (const nil)))
	 (match
	  '(radio
	    (string :tag "Match" :doc "Match for string-in-message")
	    (const nil)))
	 (info-func
	  '(function :doc "Function whose return value is ignore info"))
	 (user-func
	  '(function :doc "Function whose return value is ignore user list"))
	 (name
	  '(repeat :tag "Name" (string :doc "Ignore user name")))
	 (spec
	  (` (list :value (nil nil)
		   (choice :tag "Message"
		    (cons (, type) (, match)) (const nil) (, info-func))
		   (choice :tag "User" (const nil) (, user-func) (, name)))))
	 (args (` ((, spec)))))
    (widget-put widget :args args)
    widget))

(defcustom liece-ignore-list nil
  "A list of buffers that don't adhere to ignore-list."
  :type 'liece-ignore-spec
  :group 'liece-ignore)

(defcustom liece-compress-changes t
  "Set to t if we want instant compressed messages in the old format."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-compress-treshold 1
  "Number of lines to search back for a previous matching join/part/quit/mode."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-compress-mode-length 3
  "Number of mode compression length."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-buffer-max-size 4242424
  "Maximum size of buffers, the number of characters."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-buffer-min-size (- liece-buffer-max-size 200000)
  "Minimum size of buffers, the number of characters."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-buffer-check-interval (* 60 10)
  "Interval between `buffer-size' checks."
  :type 'integer
  :group 'liece-vars)

(static-when (featurep 'xemacs)
  (define-obsolete-variable-alias 'liece-buffer-maxsize
    'liece-buffer-max-size)
  (define-obsolete-variable-alias 'liece-buffer-defsize
    'liece-buffer-min-size)
  (define-obsolete-variable-alias 'liece-checkbuffer-interval
    'liece-buffer-check-interval))

(defcustom liece-buffer-dispose-function (function bury-buffer)
  "Function called after the buffer was disposed of."
  :type 'function
  :group 'liece-vars)

(defcustom liece-private-window-height 4
  "How tall is the window for private messages when shown."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-format-time-function
  'liece-compose-time-string
  "Function to convert `current-time-string' to the human readable form.
If you like to have the short format, set this to nil or to a
funtion (defun my-format (s) s)."
  :type 'function
  :group 'liece-vars)

(defcustom liece-keep-buffers t
  "When (re)starting IRC, erase contents of old buffers."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-scroll-step nil
  "The number of lines to try scrolling a window by when point moves out."
  :type '(radio (integer :tag "Scroll Step")
		(const nil))
  :group 'liece-look)

(defcustom liece-swap-private nil
  "When showing private is it between under dialogue or not."
  :type 'boolean
  :group 'liece-vars)

(defcustom liece-buffer-preferences
  (list
   (cons ".*" liece-D-buffer))
  "List of lists that contain regexp to match and `buffer-list' to insert.
Setting this as
(list
 (cons \"#report\" (list \"*report-buffer*\"))
 (cons \".*\" liece-D-buffer))
would cause messages from and to channel #report to be displayed on
buffer named *report-buffer* and all other messages are displayed on
dialogue-buffer."
  :type '(repeat (cons (string :tag "Channel Name")
                       (list (variable :tag "Buffe or Name"))))
  :group 'liece-vars)

(defcustom liece-channel-conversion-map nil
  "The map of channel conversion."
  :type '(repeat (cons (string :tag "Channel Name")
		       (string :tag "Channel Name (After Conversion)")))
  :group 'liece-vars)

(defcustom liece-channel-conversion-default-mask "*.jp"
  "The default mask of channel conversion."
  :type 'string
  :group 'liece-vars)

(defcustom liece-channel-id-length 5
  "Length of channel ID for ! channels."
  :type 'integer
  :group 'liece-vars)

(defcustom liece-insert-environment-version nil
  "If non-nil, display version description of current environment."
  :type 'boolean
  :group 'liece-vars)
  
(provide 'liece-vars)

;;; liece-vars.el ends here
