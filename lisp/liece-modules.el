;;; liece-modules.el --- Module definitions.
;; Copyright (C) 1999 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1999-04-12
;; Keywords: IRC, liece, APEL

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(defvar liece-modules-to-compile
  '(gettext
    liece-clfns
    liece-handler
    liece-compat
    liece-version
    liece-vars
    liece-globals
    liece-inlines
    liece-filter
    liece-coding
    liece-dcc
    liece-menu
    liece-000
    liece-200
    liece-300
    liece-400
    liece-500
    liece-nick
    liece-channel
    liece-commands
    liece-ctcp
    liece-q-el
    liece-message
    liece-handle
    liece-hilit
    liece-intl
    liece-mail
    liece-minibuf
    liece-misc
    liece-tcp
    liece-url
    liece-x-face
    liece-window
    liece))

(if (featurep 'xemacs)
    (push 'liece-xemacs liece-modules-to-compile)
  (push 'liece-emacs liece-modules-to-compile))

(if (fboundp 'set-face-stipple)
    (push 'bitmap-stipple liece-modules-to-compile))

;; (require 'pccl)
;; (unless-broken ccl-usable
;;   (push 'liece-q-ccl liece-modules-to-compile))

(setq liece-modules (cons 'liece-setup liece-modules-to-compile))

(push 'queue-m liece-modules-to-compile)

(provide 'liece-modules)

;;; liece-modules.el ends here
