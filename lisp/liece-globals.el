;;; liece-globals.el --- Global variables and constants.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

;;; Miscellaneous global variables:
(defvar liece-server-name nil
  "Primary server name.")
(defvar liece-server-process nil
  "Primary server process.")
(defvar liece-server-process-alist nil
  "An alist mapping secondary server name to opened processes.")

(defvar liece-current-channel nil
  "The channel you currently have joined.")
(defvar liece-current-channels nil
  "The channels you have currently joined.")
(defvar liece-current-chat-partner nil
  "The person you are in a private conversation with.")
(defvar liece-current-chat-partners nil
  "A list of nicknames you are chatting with.")
(defvar liece-channel-unread-list nil
  "A list of channels which has new messages.")

(defvar liece-nick-alist nil
  "An alist containing the nicknames of users known to be on IRC.
Each element is a list whose car is a nickname.")
(defvar liece-channel-alist nil
  "An alist containing the channels on IRC.
Each element is a list whose car is a channel name.")
(defvar liece-operator-alist nil
  "An alist of operators on channel.
Each element is a list whose car is a nickname.")

(defvar liece-save-variables-are-dirty nil
  "Non nil if the variables in `liece-saved-forms' are changed.")

(defvar liece-long-reply-max 38
  "Threshold of long response from the server.")

(defvar liece-polling 0
  "Interval for polling the server.")

;;; Variables local to the server buffers:
(defvar liece-real-nickname nil
  "Your nickname the server offers.
Local to the server buffers.")
(defvar liece-last-nickname nil
  "The last nickname you requested.
Local to the server buffers.")
(defvar liece-nick-accepted nil
  "The flag your nickname is accepted by the server.
Possible values are nil, `ok', and `sent'.
Local to the server buffers.")
(defvar liece-real-server-name nil
  "The server name offered by the server.
Local to the server buffers.")
(defvar liece-real-userhost nil
  "Your hostname the server offers.
Local to the server buffers.")
(defvar liece-user-at-host ""
  "The user@host for the current input.
Local to the server buffers.")
(defvar liece-user-at-host-type nil
  "The authentication type of `liece-user-at-host'.
Possible values are 'ok 'not-verified 'fake or 'invalid.
Local to the server buffers.")
(defvar liece-supported-user-mode-alist nil
  "User modes supported by server.
Local to the server buffers.")
(defvar liece-supported-channel-mode-alist nil
  "Channel modes supported by server.
Local to the server buffers.")
(defvar liece-channel-filter ""
  "Filter of the result of NAMES or LIST.
This enables us to use \\[universal-argument] with NAMES and TOPIC.
Local to the server buffers.")
(defvar liece-long-reply-count 0
  "Count of long inputs from the server.
Local to the server buffers.")
(defvar liece-obarray nil
  "Namespace of the IRC world.
Local to the server buffers.")

;;; Variables local to the command buffer:
(defvar liece-default-channel-candidate nil
  "A channel name used as completion candidate.
Local to the command buffer.")
(defvar liece-last-friends nil)
(defvar liece-last-who-expression nil)
(defvar liece-last-chat-partner nil)
(defvar liece-command-buffer-mode 'channel
  "Command buffer mode.
Possible values are `chat' and `channel'.
Local to the command buffer.")

;;; Variables local to the channel buffers:
(defvar liece-beep nil
  "If non-nil, change `liece-beep-indicator' when messages containing
a bell character arrived.
Local to the channel buffers.")
(defvar liece-freeze nil
  "If non-nil, channel window is not scrolled.
Local to the channel buffers.")
(defvar liece-own-freeze nil
  "If non-nil, channel window is not scrolled until you input.
Local to the channel buffers.")

;;; Modeline indicators:
(defvar liece-channel-status-indicator "")

(defvar liece-channel-indicator "No channel"
  "A modeline indicator of the current channel.")
(defvar liece-channels-indicator "No channel"
  "The current joined channels, \"pretty-printed.\".")
(defvar liece-command-buffer-mode-indicator nil
  "A modeline indicator for private conversation.")

(defvar liece-away-indicator "-")
(defvar liece-beep-indicator nil)
(defvar liece-freeze-indicator nil)
(defvar liece-own-freeze-indicator nil)

;;; Buffers:
(defvar liece-command-buffer "*Commands*"
  "Name of command input buffer.")
(defvar liece-dialogue-buffer "*Dialogue*"
  "Name of dialogue output buffer.")
(defvar liece-private-buffer "*Private*"
  "Name of private message buffer.")
(defvar liece-others-buffer "*Others*"
  "Name of others message buffer.")
(defvar liece-channel-buffer "*Channel*"
  "Name of Channel message buffer.")
(defvar liece-channel-buffer-format " *Channel:%s*"
  "Format of Channel message buffer.")
(defvar liece-channel-list-buffer "*Channels*"
  "Name of Channel list buffer.")
(defvar liece-nick-buffer " *Nicks*"
  "Name of nick list message buffer.")
(defvar liece-nick-buffer-format " *Nicks:%s*"
  "Format of nick list buffer.")
(defvar liece-KILLS-buffer " *KILLS*")
(defvar liece-IGNORED-buffer " *IGNORED*")
(defvar liece-WALLOPS-buffer " *WALLOPS*")

;;; Buffer lists:
(defvar liece-channel-buffer-alist nil)
(defvar liece-nick-buffer-alist nil)

(defvar liece-buffer-list nil
  "A list of buffers used in displaying messages.")

(defvar liece-D-buffer (list liece-dialogue-buffer)
  "A list of buffer where normal dialogue is sent.")
(defvar liece-O-buffer (list liece-others-buffer)
  "A list of buffer where other messages are sent.")
(defvar liece-P-buffer
  (list liece-dialogue-buffer liece-private-buffer liece-others-buffer)
  "A list of buffers where private messages to me are sent.")
(defvar liece-I-buffer (list liece-IGNORED-buffer)
  "A list of buffers where private messages to me are sent.")
(defvar liece-W-buffer (list liece-WALLOPS-buffer)
  "A list of buffers where WALLOPS messages to me are sent.")
(defvar liece-K-buffer (list liece-KILLS-buffer)
  "A list of buffers where KILL messages to me are sent.")
(defvar liece-000-buffer
  (list liece-dialogue-buffer liece-others-buffer)
  "A list of buffers where 000 messages to me are sent.")
(defvar liece-200-buffer
  (list liece-dialogue-buffer liece-others-buffer)
  "A list of buffers where 200 messages to me are sent.")
(defvar liece-300-buffer
  (list liece-dialogue-buffer liece-others-buffer)
  "A list of buffers where 300 messages to me are sent.")
(defvar liece-400-buffer
  (list liece-dialogue-buffer liece-others-buffer)
  "A list of buffers where 400 messages to me are sent.")
(defvar liece-500-buffer
  (list liece-dialogue-buffer liece-others-buffer)
  "A list of buffers where 500 messages to me are sent.")

(provide 'liece-globals)

;;; liece-globals.el ends here
