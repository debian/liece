;;; liece-version.el --- Version information for Liece.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1999-12-23
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

;;; @ version number constants
;;;

(require 'product)
(provide 'liece-version)

(product-provide 'liece-version
  (product-define "Liece" nil '(2 0 0)))

(defvar utf-2000-version)
(defvar mule-version)

(defconst liece-environment-version
  (concat
   (if (fboundp 'apel-version)
       (concat (apel-version) " ")
     nil)
   (if (featurep 'xemacs)
       (concat (cond
		((featurep 'utf-2000)
		 (concat "UTF-2000-MULE/" utf-2000-version))
		((featurep 'mule) "MULE"))
	       " XEmacs"
	       (if (string-match "^[0-9]+\\(\\.[0-9]+\\)" emacs-version)
		   (concat
		    "/"
		    (substring emacs-version 0 (match-end 0))
		    (cond ((and (boundp 'xemacs-betaname)
				xemacs-betaname)
			   ;; It does not exist in XEmacs
			   ;; versions prior to 20.3.
			   (concat " " xemacs-betaname))
			  ((and (boundp 'emacs-patch-level)
				emacs-patch-level)
			   ;; It does not exist in FSF Emacs or in
			   ;; XEmacs versions earlier than 21.1.1.
			   (format " (patch %d)" emacs-patch-level))
			  (t ""))
		    " (" xemacs-codename ") ("
		    system-configuration ")")
		 " (" emacs-version ")"))
     (let ((ver (if (string-match "\\.[0-9]+$" emacs-version)
		    (substring emacs-version 0 (match-beginning 0))
		  emacs-version)))
       (if (featurep 'mule)
	   (if (boundp 'enable-multibyte-characters)
	       (concat "Emacs/" ver
		       " (" system-configuration ")"
		       (if enable-multibyte-characters
			   (concat " MULE/" mule-version)
			 " (with unibyte mode)")
		       (if (featurep 'meadow)
			   (let ((mver (Meadow-version)))
			     (if (string-match "^Meadow-" mver)
				 (concat " Meadow/"
					 (substring mver
						    (match-end 0))))))
		       (if (boundp 'NEMACS)
			   (let ((nemacs-version
				  (condition-case ()
				      (eval '(nemacs-version))
				    (error ""))))
			     (when (string-match
				    "Nemacs version \\([^ ]*\\)"
				    nemacs-version)
			       (setq nemacs-version
				     (match-string 1 nemacs-version)))
			     (concat " NEmacs/" nemacs-version))))
	     (concat "MULE/" mule-version
		     " (based on Emacs " ver ")"))
	 (concat "Emacs/" ver " (" system-configuration ")"))))))

(defun liece-version ()
  "Print Liece version."
  (interactive)
  (let ((product-info
	 (concat (product-string-1 'liece-version t)
		 (if liece-insert-environment-version
		     (concat " " liece-environment-version)
		   nil))))
    (if (interactive-p)
	(message "%s" product-info)
      product-info)))

;;; liece-version.el ends here
