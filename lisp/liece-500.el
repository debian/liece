;;; liece-500.el --- Handler routines for 500 numeric reply.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile
  (require 'liece-inlines)
  (require 'liece-intl)
  (require 'liece-misc))

(defun* liece-handle-500-messages (number prefix rest)
  (or (string-match "[^ ]* \\([^ :]*\\) *\\([^ :]*\\) *:\\(.*\\)" rest)
      (return-from liece-handle-500-messages))
  (let ((target1 (match-string 1 rest)) (target2 (match-string 2 rest))
	(msg (match-string 3 rest)))
    (cond ((string-equal target1 "")
	   (liece-insert-error liece-500-buffer (concat msg "\n")))
	  ((string-equal target2 "")
	   (liece-insert-error liece-500-buffer
				(concat msg " (" target1 ")\n")))
	  (t
	   (liece-insert-error liece-500-buffer
				(format "%s %s (%s)\n"
					target1 msg target2))))))

(provide 'liece-500)

;;; liece-500.el ends here
