;;; liece-compat.el --- Provide compatibility for various emacsen.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1999-12-19
;; Keywords: IRC, liece, APEL

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile (require 'cl))

(require 'pcustom)
(require 'wid-edit)

(defalias 'liece-widget-convert-button 'widget-convert-button)
(defalias 'liece-widget-button-click 'widget-button-click)

(static-if (fboundp 'region-active-p)
    (defalias 'liece-region-active-p 'region-active-p)
  (defun liece-region-active-p ()
    "Return non-nil if the region is active.
If `zmacs-regions' is true, this is equivalent to `region-exists-p'.
Otherwise, this function always returns false.
\[XEmacs emulating function]"
    (and transient-mark-mode mark-active)))

(defun liece-map-overlays (function)
  "Map FUNCTION over the extents which overlap the current buffer."
  (let* ((overlayss (overlay-lists))
	 (buffer-read-only nil)
	 (overlays (delq nil (nconc (car overlayss) (cdr overlayss)))))
    (dolist (overlay overlays)
      (funcall function overlay))))

(defun liece-kill-all-overlays ()
  "Delete all overlays in the current buffer."
  (liece-map-overlays #'delete-overlay))

(defmacro liece-get-buffer-window (buffer)
  "Traverse all frames and return a window currently displaying BUFFER."
  `(get-buffer-window ,buffer t))

(static-if (fboundp 'window-displayed-height)
    (defalias 'liece-window-height 'window-displayed-height)
  (defalias 'liece-window-height 'window-height))

(static-if (fboundp 'string-to-list)
    (defalias 'liece-string-to-list 'string-to-list)
  ;; Rely on `string-to-char-list' emulation is provided in poem.
  (defalias 'liece-string-to-list 'string-to-char-list))

(defalias 'liece-mode-line-buffer-identification 'identity)

(defun liece-suppress-mode-line-format ()
  "Remove unnecessary information from `mode-line-format'."
  (let ((value (rassq 'mode-line-modified mode-line-format)))
    (if value
	(setq mode-line-format (delq value (copy-sequence mode-line-format)))
      mode-line-format)))

(defun liece-locate-data-directory (name &optional dir-list)
  "Locate a directory in a search path DIR-LIST (a list of directories)."
  (let ((dir-list
	 (or dir-list
	     (cons data-directory
		   (mapcar (lambda (path) (concat path "etc/"))
			   load-path))))
	dir)
    (while dir-list
      (if (and (car dir-list)
	       (file-directory-p
		(setq dir (concat
			   (file-name-directory
			    (directory-file-name (car dir-list)))
			   name "/"))))
	  (setq dir-list nil)
	(setq dir-list (cdr dir-list))))
    dir))

(defvar-maybe completion-display-completion-list-function
  'display-completion-list)
  
(defalias-maybe 'easy-menu-add-item 'ignore)
  
;; from XEmacs's minibuf.el
(defun-maybe temp-minibuffer-message (m)
  (let ((savemax (point-max)))
    (save-excursion
      (goto-char (point-max))
      (message nil)
      (insert m))
    (let ((inhibit-quit t))
      (sit-for 2)
      (delete-region savemax (point-max)))))

(defvar liece-read-passwd nil)
(defun liece-read-passwd (prompt)
  (if (not liece-read-passwd)
      (if (functionp 'read-passwd)
	  (setq liece-read-passwd 'read-passwd)
	(if (load "passwd" t)
	    (setq liece-read-passwd 'read-passwd)
	  (autoload 'ange-ftp-read-passwd "ange-ftp")
	  (setq liece-read-passwd 'ange-ftp-read-passwd))))
  (funcall liece-read-passwd prompt))

;; XEmacs.
(defun-maybe replace-in-string (str regexp newtext &optional literal)
  "Replace all matches in STR for REGEXP with NEWTEXT string,
 and returns the new string.
Optional LITERAL non-nil means do a literal replacement.
Otherwise treat `\\' in NEWTEXT as special:
  `\\&' in NEWTEXT means substitute original matched text.
  `\\N' means substitute what matched the Nth `\\(...\\)'.
       If Nth parens didn't match, substitute nothing.
  `\\\\' means insert one `\\'.
  `\\u' means upcase the next character.
  `\\l' means downcase the next character.
  `\\U' means begin upcasing all following characters.
  `\\L' means begin downcasing all following characters.
  `\\E' means terminate the effect of any `\\U' or `\\L'."
  (if (> (length str) 50)
      (with-temp-buffer
	(insert str)
	(goto-char 1)
	  (while (re-search-forward regexp nil t)
	    (replace-match newtext t literal))
	  (buffer-string))
  (let ((start 0) newstr)
    (while (string-match regexp str start)
      (setq newstr (replace-match newtext t literal str)
	    start (+ (match-end 0) (- (length newstr) (length str)))
	    str newstr))
    str)))

(provide 'liece-compat)

;;; liece-compat.el ends here
