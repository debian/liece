;;; liece-url.el --- URL browsing.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile
  (require 'liece-inlines)
  (require 'liece-intl)
  (require 'liece-menu))

(require 'delegate)

(defgroup liece-url nil
  "URL Browsing in IRC buffer."
  :group 'liece-vars)

(defcustom liece-url-regexp  "\\b\\(s?https?\\|ftp\\|file\\|gopher\\|news\\|telnet\\|wais\\|mailto\\):\\(//[-a-zA-Z0-9_.]+:[0-9]*\\)?[-a-zA-Z0-9_=?#$@~`%&*+|\\/.,:;]*[-a-zA-Z0-9_=#$@~`%&*+|\\/]"
  "Regular expression that matches URLs."
  :group 'liece-url
  :type 'regexp)

(define-delegated-variable-alias 'browse-url-browser-function
    'liece-url-browser-function
  "browse-url")
(defcustom liece-url-browser-function
  (delegated-variable-get-safe 'liece-url-browser-function)
  "Default URL Browser."
  :group 'liece-url)

(defcustom liece-url-alist nil
  "Alist for URL completion."
  :type 'alist
  :group 'liece-url)

(defcustom liece-url-regexp-alist nil
  "Mapping from reegxp to URL.
This alist is used for defining alternate URL abbrevs.
For example:
  (setq liece-url-regexp-alist
        '((\"\\\\bBug#\\\\([0-9][0-9]*\\\\)\\\\b\" . \"http://bugs.debian.org/\\\\1\")))"
  :type 'alist
  :group 'liece-url)

(define-widget 'liece-url-link 'link
  "A link to an www page."
  :help-echo 'widget-url-link-help-echo
  :action 'liece-url-link-action)

(defun liece-url-link-action (widget &optional event)
  (let ((browse-url-browser-function liece-url-browser-function))
    (browse-url (widget-value widget))))

(defvar liece-add-url-functions nil)

(defun liece-url-add-buttons (start end)
  (save-excursion
    (let ((pairs (cons (cons liece-url-regexp t)
		       liece-url-regexp-alist)))
      (dolist (pair pairs)
	(goto-char start)
	(while (re-search-forward (car pair) end t)
	  (let ((url (match-string 0)))
	    (if (stringp (cdr pair))
		(setq url (save-match-data
			    (string-match (car pair) url)
			    (replace-match (cdr pair) nil nil url)))
	      (if (functionp (cdr pair))
		  (setq url (funcall (cdr pair) url))))
	    (if liece-highlight-mode
		(liece-widget-convert-button
		 'url-link (match-beginning 0) (match-end 0) url))
	    (unless (assoc url liece-url-alist)
	      (push (list url) liece-url-alist)
	      (run-hook-with-args 'liece-add-url-functions url))))))))

(defun liece-command-browse-url (&optional url)
  (interactive
   (let (url)
     (if (and current-prefix-arg (eq current-prefix-arg '-))
	 (setq url (caar liece-url-alist))
       (setq url (completing-read (_ "Open URL:") liece-url-alist)))
     (list url)))
  (let ((browse-url-browser-function liece-url-browser-function))
    (browse-url url)))

(defun url-irc-liece (host port channel user password)
  (let ((liece-server
	 `(:host ,host :service ,port :password ,password))
	(liece-startup-channel channel))
    (liece)))

(provide 'liece-url)

;;; liece-url.el ends here
