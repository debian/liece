;;; liece-x-face.el --- X-Face wrappers.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile (require 'liece-compat))

(require 'path-util)

(defvar liece-x-face-insert-function
  (when (and (module-installed-p 'bitmap) (module-installed-p 'x-face))
    (function liece-x-face-insert-with-bitmap)))

(eval-and-compile
  (autoload 'x-face-encode "x-face")
  (autoload 'x-face-read-existing-file-name "x-face")
  (autoload 'x-face-icons-to-xbm "x-face")
  (autoload 'x-face-x-face-encoded-string-to-icon-string "x-face")
  (autoload 'bitmap-insert-xbm-buffer "bitmap")
  (autoload 'bitmap-decode-xbm "bitmap")
  (autoload 'bitmap-read-xbm-buffer "bitmap")
  (autoload 'bitmap-compose "bitmap"))
  
(defalias 'liece-x-face-encode 'x-face-encode)
(defalias 'liece-x-face-read-existing-file-name
  'x-face-read-existing-file-name)

(defun liece-x-face-insert-with-bitmap (buffer str nick)
  (save-excursion
    (set-buffer buffer)
    (goto-char (point-max))
    (let* (buffer-read-only
	   (buf (x-face-icons-to-xbm
		 nick 1 1
		 (x-face-x-face-encoded-string-to-icon-string str)))
	   (cmp (bitmap-decode-xbm (bitmap-read-xbm-buffer buf)))
	   (len (length cmp)) (col (current-column))
	   (prefix (buffer-substring
		    (line-beginning-position) (point))) pt)
      (delete-region (line-beginning-position) (point))
      (dotimes (i len)
	(insert ?\n)
	(if (= i (/ len 2))
	    (insert prefix)
	  (move-to-column col t))
	(setq pt (point))
	(insert (bitmap-compose (aref cmp i)))
	(overlay-put
	 (make-overlay pt (point)) 'face 'liece-client-face)))))
  
(defun liece-x-face-insert (buffers str nick)
  (cond
   ((or (not buffers) (listp buffers))
    (dolist (buffer buffers)
      (liece-x-face-insert buffer str nick)))
   ((fboundp liece-x-face-insert-function)
    (funcall liece-x-face-insert-function buffers str nick))))

(provide 'liece-x-face)

;;; liece-x-face.el ends here
