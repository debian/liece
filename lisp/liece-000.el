;;; liece-000.el --- Handler routines for 000 numeric reply.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-01-26
;; Keywords: IRC, liece

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile (require 'cl))

(eval-when-compile
  (require 'liece-inlines)
  (require 'liece-intl)
  (require 'liece-misc))

(require 'liece-version)

(defun* liece-handle-000-messages (number prefix rest)
  (setq liece-nick-accepted 'ok)
  (or (string-match "[^ ]* \\([^ :]*\\) *\\([^ :]*\\) *:\\(.*\\)" rest)
      (return-from liece-handle-000-messages))
  (let ((target1 (match-string 1 rest)) (target2 (match-string 2 rest))
	(msg (match-string 3 rest)))
    (cond
     ((string-equal target1 "")
      (liece-insert liece-000-buffer
		     (concat liece-info-prefix msg "\n")))
     ((string-equal target2 "")
      (liece-insert liece-000-buffer
		     (format "%s%s (%s)\n" liece-info-prefix msg target1)))
     (t
      (liece-insert liece-000-buffer
		     (format "%s%s %s (%s)\n"
			     liece-info-prefix target1 msg target2))))))

(defun* liece-handle-001-message (prefix rest)
  "RPL_WELCOME \"Welcome to the Internet Relay Network <nick>\""
  (or (< 0 (length (setq rest (liece-split-line rest))))
      (return-from liece-handle-001-message))
  (let ((nick (car rest)))
    (setq liece-real-server-name prefix
	  liece-real-nickname nick
	  liece-real-userhost nil)
    (push (list liece-real-nickname) liece-nick-alist)
    (liece-send "USERHOST %s" liece-real-nickname)
    (liece-insert-info
     liece-000-buffer
     (format
      (_ "Welcome to the Internet Relay Chat world. Your nick is %s.\n")
      nick))))

(defun liece-handle-002-message (prefix rest)
  "RPL_YOURHOST \"Your host is <host>, running version <version>\"."
  (cond
   ((string-match "running version \\(.*\\)" rest)
    (liece-insert-info liece-000-buffer
			(format (_ "Your server is %s (version %s).\n")
				liece-real-server-name (match-string 1 rest))))
   (t
    (liece-insert-info liece-000-buffer
			(format (_ "Your client version is %s.\n")
				(liece-version))))))

(defun liece-handle-003-message (prefix rest)
  "RPL_CREATED \"This server was created <time>\"."
  (if (string-match "was created \\(.*\\)" rest)
      (liece-insert-info liece-000-buffer
			  (format (_ "Your server was created %s\n")
				  (match-string 1 rest)))))

(defmacro char-list-to-string-alist (clist)
  `(mapcar
    (lambda (ch) (list (char-to-string ch)))
    ,clist))

(defun* liece-handle-004-message (prefix rest)
  "RPL_MYINFO \"<umodes> <chnlmodes>\""
  (or (string-match "[^ ]* \\(.*\\)" rest)
      (return-from liece-handle-004-message))
  (let ((rest (match-string 1 rest)))
    (cond
     ((string-match "[^ ]* [^ ]* \\([^ ]+\\) \\(.*\\)" rest)
      (setq liece-supported-user-mode-alist
	    (char-list-to-string-alist
	     (liece-string-to-list (match-string 1 rest)))
	    liece-supported-channel-mode-alist
	    (char-list-to-string-alist
	     (liece-string-to-list (match-string 2 rest)))))
     (t
      (liece-insert-info liece-000-buffer (concat rest "\n"))))))


;; Undernet's MAP feature
(defvar liece-undernet-map nil)

(defun liece-handle-005-message (prefix rest)
  "RPL_MAP \"<server>\"."
  (liece-increment-long-reply-count)
  (liece-check-long-reply-count)
  (push rest liece-undernet-map))

(defun liece-handle-006-message (prefix rest)
  "RPL_MAPMORE \"<server> --> *more*\"."
  (liece-increment-long-reply-count)
  (liece-check-long-reply-count)
  (if (string-match " --> \*more\*" rest)
      (setq rest (concat "[" (substring rest 0 (match-beginning 0)))) "]*")
  (push rest liece-undernet-map))

(defun liece-handle-007-message (prefix rest)
  "RPL_MAPEND \"End of /MAP\"."
  (liece-reset-long-reply-count)
  (dolist (map liece-undernet-map)
    (liece-insert-info liece-000-buffer (concat map "\n")))
  (setq liece-undernet-map nil))


;;; @ register message handlers
;;;

(eval-when-compile (require 'liece-handler))

(liece-handler-define-backend "000")

(defmacro liece-register-000-handler (num)
  `(progn
     (liece-handler-define-function
      ,(format "%03d" num) '(prefix require "000")
      ',(intern (format "liece-handle-%03d-message" num)))
     (defvar ,(intern (format "liece-%03d-hook" num)) nil)
     (defvar ,(intern (format "liece-after-%03d-hook" num)) nil)))

(liece-register-000-handler 001)
(liece-register-000-handler 002)
(liece-register-000-handler 003)
(liece-register-000-handler 004)
(liece-register-000-handler 005)
(liece-register-000-handler 006)
(liece-register-000-handler 007)

(provide 'liece-000)

;;; liece-000.el ends here
