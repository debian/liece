;;; liece-coding.el --- Converting string with apropriate coding system.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1999-06-02
;; Keywords: IRC, liece, coding-system, MULE

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(require 'poem)
(require 'mcharset)

(eval-when-compile (require 'liece-compat))

(defgroup liece-coding nil
  "Code conversion group"
  :tag "Coding"
  :prefix "liece-"
  :group 'liece)
  
(defcustom liece-mime-charset-for-write
  (if (mime-charset-to-coding-system 'iso-2022-jp-2)
      'iso-2022-jp-2
    default-mime-charset-for-write)
  "Charset used in any transferred messages."
  :type 'mime-charset
  :group 'liece-coding)

(defcustom liece-mime-charset-for-read
  (if (mime-charset-to-coding-system 'x-ctext)
      'x-ctext
    default-mime-charset)
  "Charset used in any transferred messages."
  :type 'mime-charset
  :group 'liece-coding)

(static-when (featurep 'xemacs)
  (define-obsolete-variable-alias 'liece-default-mime-charset
    'liece-mime-charset-for-write)
  (define-obsolete-variable-alias 'liece-default-mime-charset-for-read
    'liece-mime-charset-for-read))

(defcustom liece-detect-coding-system nil
  "Whether use coding system auto detection."
  :type 'boolean
  :group 'liece-coding)

(defcustom liece-detect-coding-region-function
  (function liece-detect-coding-region)
  "User customizable `detect-coding-region'."
  :type 'function
  :group 'liece-coding)

(defcustom liece-detect-coding-string-function
  (function liece-detect-coding-string)
  "User customizable detect-coding-string."
  :type 'function
  :group 'liece-coding)

(defcustom liece-decode-coding-string-function
  (function decode-coding-string)
  "User customizable `decode-coding-string'."
  :type 'function
  :group 'liece-coding)

(defun liece-coding-encode-charset-region (start end &optional lbt)
  (encode-mime-charset-region start end liece-mime-charset-for-write lbt))

(defun liece-coding-decode-charset-region (start end)
  (let ((cs (if (and liece-detect-coding-system
		     (fboundp liece-detect-coding-region-function))
		(funcall liece-detect-coding-region-function start end)
	      (mime-charset-to-coding-system liece-mime-charset-for-read))))
    (decode-coding-region start end cs)))

(defun liece-detect-coding-region (start end)
  (let ((cs (detect-coding-region start end)))
    (if (listp cs)
	(setq cs (car cs)))
    (if (featurep 'xemacs)
	(eval '(setq cs (coding-system-name cs))))
    cs))

(defun liece-coding-encode-charset-string (str &optional lbt)
  (encode-mime-charset-string str liece-mime-charset-for-write lbt))

(eval-and-compile
  (when (fboundp 'detect-coding-string)
    (defun liece-detect-coding-string (str)
      (let ((cs (detect-coding-string str)))
	(if (listp cs)
	    (setq cs (car cs)))
	(static-if (fboundp 'coding-system-name)
	    (coding-system-name cs)
	  cs)))))

(defun liece-coding-decode-charset-string (str)
  (let ((cs (or (and liece-detect-coding-system
		     (fboundp liece-detect-coding-string-function)
		     (funcall liece-detect-coding-string-function str))
		(mime-charset-to-coding-system liece-mime-charset-for-read))))
    (funcall liece-decode-coding-string-function str cs)))

(provide 'liece-coding)

;;; liece-coding.el ends here
