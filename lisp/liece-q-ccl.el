;;; liece-q-ccl.el --- CTCP binary data quotation in CCL.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1999-01-31
;; Revised: 1999-01-31
;; Keywords: IRC, liece, CTCP

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(require 'broken)
(require 'pccl)
(require 'poem)				;char-int

(eval-and-compile
  (defconst liece-quote-ccl-256-table
    (let ((i 0)
	  table)
      (while (< i 256)
	(setq table (nconc table (list i))
	      i (1+ i)))
      table)))

(broken-facility ccl-cascading-write
  "Emacs CCL write command does not accept more than 2 arguments."
  (condition-case nil
      (progn
	(define-ccl-program cascading-read-test
	  '(1
	    (write r0 r1 r2)))
	t)
    (error nil)))
 
(define-ccl-program liece-quote-ccl-decode
  `(1
    (loop
      (read-if
       (r0 == ?\\)
       ((read-if
	 (r1 == ?\\)
	 (write r1)
	 (branch
	  r1
	  ,@(mapcar
	     (lambda (r1)
	       (cond
		((= r1 (char-int ?a))
		 `(write ?\x01))
		((= r1 (char-int ?n))
		 `(write ?\n))
		((= r1 (char-int ?r))
		 `(write ?\r))
		(t
		 (if-broken ccl-cascading-write
		     `((write r0)
		       (write r1))
		   `(write r0 r1)))))
	     liece-quote-ccl-256-table))))
       (write r0))
      (repeat))))

(define-ccl-program liece-quote-ccl-encode
  `(2
    (loop
      (read-branch
       r0
       ,@(mapcar
	  (lambda (r0)
	    (cond
	     ((= r0 (char-int ?\\))
	      `(write-repeat "\\\\"))
	     ((= r0 (char-int ?\x01))
	      `(write-repeat "\\a"))
	     ((= r0 (char-int ?\n))
	      `(write-repeat "\\n"))
	     ((= r0 (char-int ?\r))
	      `(write-repeat "\\r"))
	     (t
	      `(write-repeat r0))))
	  liece-quote-ccl-256-table)))))

(make-ccl-coding-system
 'liece-quote-ccl-cs ?Q "CTCP Quote Decoder/Encoder"
 'liece-quote-ccl-decode 'liece-quote-ccl-encode)

(defun liece-quote-ccl-decode-string (string-to-decode)
  (decode-coding-string string-to-decode 'liece-quote-ccl-cs))

(defun liece-quote-ccl-encode-string (string-to-encode)
  (encode-coding-string string-to-encode 'liece-quote-ccl-cs))

(defun liece-quote-ccl-decode-region (min max)
  (decode-coding-region min max 'liece-quote-ccl-cs))

(defun liece-quote-ccl-encode-region (min max)
  (encode-coding-region min max 'liece-quote-ccl-cs))

(defalias 'liece-quote-decode-string 'liece-quote-ccl-decode-string)
(defalias 'liece-quote-encode-string 'liece-quote-ccl-encode-string)

(defalias 'liece-quote-decode-region 'liece-quote-ccl-decode-region)
(defalias 'liece-quote-encode-region 'liece-quote-ccl-encode-region)

(provide 'liece-q-ccl)

;;; liece-q-ccl.el ends here
