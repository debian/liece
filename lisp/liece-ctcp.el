;;; liece-ctcp.el --- CTCP handlers and commands.
;; Copyright (C) 1998-2000 Daiki Ueno

;; Author: Daiki Ueno <ueno@unixuser.org>
;; Created: 1998-09-28
;; Revised: 1998-11-25
;; Keywords: IRC, liece, CTCP

;; This file is part of Liece.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.


;;; Commentary:
;; 

;;; Code:

(eval-when-compile
  (require 'liece-inlines)
  (require 'liece-misc))

(require 'liece-version)
(require 'liece-handler)

;; (require 'pccl)
  
;; (if-broken ccl-usable
;;     (require 'liece-q-el)
;;   (require 'liece-q-ccl))
(require 'liece-q-el)

(require 'liece-x-face)

(autoload 'liece-ctcp-dcc-message "liece-dcc")

(eval-and-compile
  (defconst liece-ctcp-supported-symbols
    '(version userinfo clientinfo ping time x-face comment help)))

(defun liece-ctcp-make-menu-command-wrapper (symbol)
  (fset (intern (format "liece-menu-callback-ctcp-%s" symbol))
	`(lambda ()
	   (interactive)
	   (dolist (nick liece-nick-region-nicks)
	     (funcall (symbol-function
		       (intern (format "liece-command-ctcp-%s" ',symbol)))
		      nick)))))

(dolist (symbol liece-ctcp-supported-symbols)
  (liece-ctcp-make-menu-command-wrapper symbol))
	     
(defvar liece-ctcp-message
  (eval-when-compile
    (concat liece-client-prefix "%s(%s) = %s"))
  "Message in which info of other clients is displayed.")

(defvar liece-ctcp-buffer (append liece-D-buffer liece-O-buffer))

(defvar liece-ctcp-ping-time '(0 0 0))

(defvar liece-ctcp-last-command nil
  "The last command executed.")

(defvar liece-ctcp-last-nick nil
  "The last nick being queried.")

(defconst liece-ctcp-error-message "Unrecognized command: '%s'"
  "Error message given to anyone asking wrong CLIENT data.")

(defun liece-ctcp-last-nick-maybe-change (prefix rest)
  (if (equal prefix liece-ctcp-last-nick)
      (setq liece-ctcp-last-nick rest))
  nil)

(defun liece-ctcp-last-nick-maybe-reset (prefix rest)
  (if (equal prefix liece-ctcp-last-nick)
      (setq liece-ctcp-last-nick nil)))

(add-hook 'liece-nick-hook 'liece-ctcp-last-nick-maybe-change t)
(add-hook 'liece-quit-hook 'liece-ctcp-last-nick-maybe-reset)
  
(defcustom liece-ctcp-file-save-directory liece-directory
  "Directory to save received files."
  :type 'directory
  :group 'liece-ctcp)
  
(liece-handler-define-backend "ctcp-message")

(defmacro liece-register-ctcp-message-handler (name)
  `(liece-handler-define-function
    ,name '(from chnl data "ctcp-message")
    ',(intern (format "liece-ctcp-%s-message" name))))

(liece-register-ctcp-message-handler "version")
(liece-register-ctcp-message-handler "userinfo")
(liece-register-ctcp-message-handler "clientinfo")
(liece-register-ctcp-message-handler "ping")
(liece-register-ctcp-message-handler "time")
(liece-register-ctcp-message-handler "file")
(liece-register-ctcp-message-handler "x-face")
(liece-register-ctcp-message-handler "comment")
(liece-register-ctcp-message-handler "help")
(liece-register-ctcp-message-handler "action")
(liece-register-ctcp-message-handler "dcc")
(liece-register-ctcp-message-handler "errmsg")

(defun* liece-ctcp-message (from chnl rest)
  (or (string-match "^\\([^\001]*\\)\001\\([^\001]*\\)\001" rest)
      (return-from liece-ctcp-message))
  (let (hook after-hook data message)
    (setq data (match-string 2 rest)
	  rest (concat
		(match-string 1 rest)
		(substring rest (match-end 0))))
    (if (string-match "^\\([^ ]*\\) *:?" data)
	(setq message (downcase (match-string 1 data))
	      data (substring data (match-end 0)))
      (setq message "errmsg"
	    data (_ "Couldn't figure out what was said.")))
    (setq hook
	  (intern-soft
	   (concat "liece-ctcp-" message "-hook"))
	  after-hook
	  (intern-soft
	   (concat "liece-after-ctcp-" message "-hook")))
    (if (run-hook-with-args-until-success hook from chnl data)
	(return-from liece-ctcp-message rest))
    (let ((func
	   (liece-handler-find-function
	    message '(from chnl data) "ctcp-message")))
      (if func
	  (funcall func from chnl data)
	(liece-ctcp-messages message from chnl data))
      (run-hook-with-args after-hook from chnl data))
    rest))

(defun liece-ctcp-messages (message from chnl rest)
  (liece-send "NOTICE %s :\001ERRMSG %s :%s\001"
	       from (upcase message)
	       (format liece-ctcp-error-message
		       (upcase message)))
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert (upcase message) from chnl rest))

(defun liece-ctcp-action-message (from chnl rest)
  "CTCP ACTION handler."
  (liece-display-message  
   (liece-make-message from (liece-channel-virtual chnl) rest 'action)))

(defun liece-ctcp-insert (message from &optional chnl rest)
  (if (or (null chnl)
	  (liece-nick-equal chnl liece-real-nickname))
      (liece-message "%s query from %s." message from)
    (liece-message "%s query from %s (%s)." message from chnl)
    (liece-insert-client
     (liece-pick-buffer chnl)
     (format "%s query from %s%s\n"
	     message from (if rest (concat ":" rest) "")))))

(defun liece-ctcp-version-message (from chnl rest)
  "CTCP VERSION handler."
  (liece-send "NOTICE %s :\001VERSION %s :\001"
	      from (liece-version))
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "VERSION" from chnl rest))

(defun liece-ctcp-userinfo-message (from chnl rest)
  "CTCP USERINFO handler."
  (liece-send "NOTICE %s :\001USERINFO %s\001"
	      from liece-ctcp-userinfo)
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "USERINFO" from chnl))

(defun liece-ctcp-clientinfo-message (from chnl rest)
  "CTCP CLIENTINFO handler."
  (liece-send "NOTICE %s :\001CLIENTINFO %s\001"
	      from
	      (eval-when-compile
		(mapconcat 
		 (lambda (symbol) (upcase (symbol-name symbol)))
		 liece-ctcp-supported-symbols " ")))
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "CLIENTINFO" from chnl))

(defvar liece-ctcp-help-message
  "This is a help message for CTCP requests.
\"VERSION\" gives version of this client.
\"USERINFO\" gives user supplied information if any.
\"CLIENTINFO\" gives commands this client knows.
\"PING\" returns the arguments it receives.
\"TIME\" tells you the time on the user's host.
\"FILE\" send a small file via IRC messages.
\"X-FACE\" gives you user supplied X-Face.
\"COMMENT\" returns string sent by other person.
\"HELP\" gives this help message"
  "Help message for CTCP requests.")
  
(defun liece-ctcp-help-message (from chnl rest)
  "CTCP HELP handler."
  (liece-send
   "NOTICE %s :\001HELP %s\001"
   from (liece-quote-encode-string liece-ctcp-help-message))
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "HELP" from chnl))

(defun liece-ctcp-comment-message (from chnl rest)
  "CTCP COMMENT handler."
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "COMMENT" from chnl))

(defun liece-ctcp-ping-message (from chnl rest)
  "CTCP PING handler."
  (liece-send "NOTICE %s :\001PING %s\001" from rest)
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "PING" from chnl))

(defun liece-ctcp-time-message (from chnl rest)
  "CTCP TIME handler."
  (liece-send "NOTICE %s :\001TIME %s\001"
	      from (funcall liece-format-time-function
			    (current-time)))
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "TIME" from chnl))

(defun liece-ctcp-x-face-message (from chnl rest)
  "CTCP X-FACE handler."
  (liece-send "NOTICE %s :\001X-FACE %s\001"
	      from liece-ctcp-x-face)
  (setq chnl (liece-channel-virtual chnl))
  (liece-ctcp-insert "X-FACE" from chnl))

(liece-handler-define-backend "ctcp-notice")

(defmacro liece-register-ctcp-notice-handler (name)
  `(liece-handler-define-function
    ,name '(prefix rest "ctcp-notice")
    ',(intern (format "liece-ctcp-%s-notice" name))))

(liece-register-ctcp-notice-handler "version")
(liece-register-ctcp-notice-handler "userinfo")
(liece-register-ctcp-notice-handler "clientinfo")
(liece-register-ctcp-notice-handler "ping")
(liece-register-ctcp-notice-handler "time")
(liece-register-ctcp-notice-handler "file")
(liece-register-ctcp-notice-handler "x-face")
(liece-register-ctcp-notice-handler "comment")
(liece-register-ctcp-notice-handler "help")
(liece-register-ctcp-notice-handler "dcc")
(liece-register-ctcp-notice-handler "errmsg")

(defun* liece-ctcp-notice (prefix rest)
  (or (string-match "^\\([^\001]*\\)\001\\([^\001]*\\)\001" rest)
      (return-from liece-ctcp-notice))
  (let (hook after-hook data message)
    (setq data (match-string 2 rest)
	  rest (concat
		(match-string 1 rest)
		(substring rest (match-end 0))))
    (if (string-match "^\\([^ ]*\\) *:?" data)
	(setq message (downcase (match-string 1 data))
	      data (substring data (match-end 0)))
      (setq message "errmsg"
	    data (_ "Couldn't figure out what was said.")))
    (setq hook
	  (intern-soft
	   (concat "liece-ctcp-" message "-notice-hook"))
	  after-hook
	  (intern-soft
	   (concat "liece-after-ctcp-" message "-notice-hook")))
    (if (run-hook-with-args-until-success hook prefix data)
	(return-from liece-ctcp-notice rest))
    (let ((func
	   (liece-handler-find-function
	    message '(prefix data) "ctcp-notice")))
      (if func
	  (funcall func prefix data)
	(liece-ctcp-notices message prefix data)))
    (run-hook-with-args after-hook prefix data)
    rest))

(defun liece-ctcp-notices (message prefix rest)
  (liece-message
   (_ "Unknown ctcp notice \":%s %s %s\"")
   prefix (upcase message) rest))

(liece-handler-define-backend "ctcp-file")

(defmacro liece-register-file-handler (name)
  `(liece-handler-define-function
    ,name '(prefix name data "ctcp-file")
    ',(intern (format "liece-file-%s" name))))

(liece-register-file-handler "start")
(liece-register-file-handler "cont")
(liece-register-file-handler "end")

(defun* liece-ctcp-file-notice (prefix rest)
  (when liece-file-accept
    (multiple-value-bind (message name data)
	(liece-split-line rest)
      (setq message (downcase message))
      (let ((hook
	     (intern-soft
	      (concat "liece-file-" message "-hook")))
	    (after-hook
	     (intern-soft
	      (concat "liece-after-file-" message "-hook")))
	    func)
	(if (run-hook-with-args-until-success hook prefix name)
	    (return-from liece-ctcp-file-notice))
	(setq func (liece-handler-find-function
		    message '(prefix name data) 'ctcp-file))
	(if func
	    (funcall func prefix name data)
	  (liece-file-notices message prefix name data))
	(run-hook-with-args after-hook prefix name)))))

(defun liece-file-notices (message prefix name data)
  (liece-message
   (_ "Unknown FILE message \":%s %s %s %s\"")
   prefix (upcase message) name data))

(defun liece-file-start (prefix name data)
  "CTCP FILE start handler."
  (save-excursion
    (set-buffer
     (liece-get-buffer-create
      (format " *ctcp-file:%s*" name)))
    (buffer-disable-undo)
    (set-buffer-multibyte nil)
    (erase-buffer)
    (insert data)))

(defun liece-file-cont (prefix name data)
  "CTCP FILE cont handler."
  (save-excursion
    (set-buffer
     (liece-get-buffer-create
      (format " *ctcp-file:%s*" name)))
    (goto-char (point-max))
    (insert data)))

(defun liece-file-end (prefix name data)
  "CTCP FILE cont handler."
  (save-excursion
    (set-buffer
     (liece-get-buffer-create
      (format " *ctcp-file:%s*" name)))
    (goto-char (point-max))
    (insert data)
    (liece-quote-decode-region (point-min)(point-max))
    (goto-char (point-min))
    (when (or (null liece-file-confirm-save)
	      (y-or-n-p "Save file? "))
      (or (file-directory-p liece-ctcp-file-save-directory)
	  (make-directory liece-ctcp-file-save-directory))
      (write-region-as-binary
       (point-min)(point-max)
       (expand-file-name
	(file-name-nondirectory
	 (concat name "-" prefix))
	liece-ctcp-file-save-directory))
      (kill-buffer (current-buffer)))))

(defun liece-ctcp-version-insert (buffer prefix name
					 &optional version environment)
  (or (listp buffer)
      (setq buffer (list buffer)))
  (liece-insert buffer
		(concat (format liece-ctcp-message
				"VERSION" prefix "")
			name "\n"))
  (when version
    (liece-insert buffer
		  (concat (format liece-ctcp-message
				  "VERSION" prefix "")
			  "\t" version
			  (if environment
			      (concat " " environment))
			  "\n"))))

(defun liece-ctcp-version-notice (prefix rest)
  "CTCP VERSION reply handler."
  (if (null rest)
      (liece-message (_ "Empty CLIENT version notice from \"%s\".") prefix)
    (cond
     ((string-match "^\\([^:]*\\):\\([^:]+\\):?\\([^:]*\\)" rest)
      (liece-ctcp-version-insert liece-ctcp-buffer
				 prefix (match-string 1 rest)
				 (match-string 2 rest)
				 (match-string 3 rest)))
     ((string-match "^\\([^:]*\\):\\(.*\\)" rest)
      (liece-ctcp-version-insert liece-ctcp-buffer
				 prefix (match-string 1 rest)))
     (t
      (liece-ctcp-version-insert liece-ctcp-buffer prefix rest)))))

(defun liece-ctcp-clientinfo-notice (prefix rest)
  "CTCP CLIENTINFO reply handler."
  (liece-insert liece-ctcp-buffer
		 (format (concat liece-ctcp-message "\n")
			 "CLIENTINFO" prefix rest)))

(defun liece-ctcp-userinfo-notice (prefix rest)
  "CTCP USERINFO reply handler."
  (liece-insert liece-ctcp-buffer
		 (format (concat liece-ctcp-message "\n")
			 "USERINFO" prefix rest)))

(defun liece-ctcp-help-notice (prefix rest)
  "CTCP HELP reply handler."
  (liece-insert liece-ctcp-buffer
		 (format (concat liece-ctcp-message "\n")
			 "HELP" prefix rest)))

(defun liece-ctcp-x-face-notice (prefix rest)
  "CTCP X-FACE reply handler."
  (let ((buffer liece-ctcp-buffer))
    (liece-insert buffer
		   (format liece-ctcp-message
			   "X-FACE" prefix ""))
    (if (and liece-use-x-face
	     (string-match "[^ \t]" rest))
	(liece-x-face-insert
	 buffer (replace-in-string rest "[ \t\r\n]+" "") prefix)
      (liece-insert buffer rest))
    (let (liece-display-time)
      (liece-insert buffer "\n"))))

(defun liece-ctcp-errmsg-notice (prefix rest)
  "CTCP ERRMSG reply handler."
  (liece-insert liece-ctcp-buffer
		 (format (concat liece-ctcp-message "\n")
			 "ERRMSG" prefix rest)))

(defun liece-ctcp-comment-notice (from rest)
  "CTCP COMMENT reply handler."
  (liece-insert liece-ctcp-buffer
		 (format (concat liece-ctcp-message "\n")
			 "COMMENT" from rest))
  (liece-message "COMMENT query from %s." from))

(defmacro liece-ctcp-prepare-ping-seconds (timenow)
  `(format (_ "%d sec")
	   (+ (* 65536 (- (car ,timenow) (car liece-ctcp-ping-time)))
	      (- (cadr ,timenow) (cadr liece-ctcp-ping-time)))))

(defun liece-ctcp-ping-notice (from rest)
  "CTCP PING reply handler."
  (let ((timenow (current-time)))
    (liece-insert liece-ctcp-buffer
		   (format (concat liece-ctcp-message "\n")
			   "PING" from
			   (liece-ctcp-prepare-ping-seconds timenow)))))

(defun liece-ctcp-time-notice (from rest)
  "CTCP TIME reply handler."
  (liece-insert liece-ctcp-buffer
		(format (concat liece-ctcp-message "\n")
			"TIME" from rest)))

(defmacro liece-complete-client ()
  '(let ((completion-ignore-case t) (nick liece-ctcp-last-nick))
     (liece-minibuffer-completing-read
      (_ "Whose client: ") liece-nick-alist nil nil nil nil
      (if nick (liece-channel-virtual nick)))))

(defun liece-minibuffer-complete-client-query ()
  (let* ((alist
	  (eval-when-compile
	    (list-to-alist
	     (mapcar
	      (lambda (symbol) (downcase (symbol-name symbol)))
	      liece-ctcp-supported-symbols))))
	 (candidate (liece-minibuffer-prepare-candidate))
	 (completion (try-completion candidate alist))
	 (all (all-completions candidate alist)))
    (liece-minibuffer-finalize-completion completion candidate all)))

(defmacro liece-complete-query ()
  '(let ((completion-ignore-case t)
	 (liece-minibuffer-complete-function
	  (function liece-minibuffer-complete-client-query)))
     (read-from-minibuffer
      (_ "Which query: ") liece-ctcp-last-command
      liece-minibuffer-map)))

(defun liece-ctcp-make-command-wrapper (symbol)
  (fset (intern (format "liece-command-ctcp-%s" symbol))
	`(lambda (client)
	   (interactive (list (liece-complete-client)))
	   (setq client (liece-channel-real client)
		 liece-ctcp-last-nick client
		 ,@(if (eq symbol 'ping)
		       '(liece-ctcp-ping-time
			 (current-time))))
	   (liece-send "PRIVMSG %s :\001%s\001"
		       client (upcase (symbol-name ',symbol))))))

(dolist (symbol liece-ctcp-supported-symbols)
  (liece-ctcp-make-command-wrapper symbol))

(defun liece-command-ctcp-action (&optional arg)
  "Send CTCP action."
  (interactive
   (if current-prefix-arg
       (list current-prefix-arg)))
  (let ((completion-ignore-case t)
	message)
    (if arg
	(setq liece-last-chat-partner 
	      (liece-channel-virtual
	       (liece-minibuffer-completing-read 
		(_ "To whom: ")
		(append liece-nick-alist liece-channel-alist)
		nil nil nil nil liece-last-chat-partner))))
    (beginning-of-line)
    (setq message (buffer-substring (point)(progn (end-of-line)(point))))
    (if (string= message "")
	(setq message (read-string "Action: "))
      (let ((next-line-add-newlines t))
	(next-line 1)))
    (liece-send "PRIVMSG %s :\001ACTION %s\001"
		 (if arg
		     liece-last-chat-partner
		   (liece-channel-real liece-current-channel))
		 message)
    (if arg
	(liece-own-private-message message 'action)
      (liece-own-channel-message message 'action))))

(define-obsolete-function-alias 'liece-command-send-action
  'liece-command-ctcp-action)

(defun liece-command-ctcp-generic (nick command)
  "Ask about someones client clientinfo."
  (interactive (list (liece-complete-client) (liece-complete-query)))
  (setq nick (liece-channel-real nick)
	liece-ctcp-last-nick nick
	liece-ctcp-last-command command)
  (if (string-equal-ignore-case liece-ctcp-last-command "ping")
      (setq liece-ctcp-ping-time (current-time)))
  (liece-send "PRIVMSG %s :\001%s\001" nick command))

(defun liece-command-ctcp-userinfo-from-minibuffer (info)
  "Ask about someones client clientinfo."
  (interactive
   (list (read-from-minibuffer "New userinfo: " liece-ctcp-userinfo)))
  (setq liece-ctcp-userinfo info))

(defun liece-command-ctcp-x-face-from-xbm-file (file)
  (interactive "fXBM File: ")
  (let (data)
    (and (file-exists-p file) (file-readable-p file)
	 (setq data (liece-x-face-encode file))
	 (setq liece-ctcp-x-face
	       (replace-in-string (cadr (nth 3 data)) "[ \t\n]" "")))))

(defun liece-command-send-file (file to)
  "Send a file to given  user."
  (interactive "fFile name: \nsTo whom: ")
  (save-excursion
    (set-buffer (liece-get-buffer-create (format " *ctcp-file:%s*" file)))
    (buffer-disable-undo)
    (set-buffer-multibyte nil)
    (erase-buffer)
    (insert-file-contents-as-binary file)
    (liece-quote-encode-region (point-min)(point-max))
    (goto-char (point-min))
    (let ((bound (min (point-max) (+ 80 (point))))
	  (liece-mime-charset-for-write 'binary))
      (liece-send
       "NOTICE %s :\001FILE START %s :%s\001"
       to file (buffer-substring (point) bound))
      (goto-char bound)
      (while (not (eobp))
	(if (= 1 (mod (point) 800))
	    (sit-for 1))
	(setq bound (min (point-max) (+ 80 (point))))
	(liece-send "NOTICE %s :\001FILE CONT %s :%s\001"
		    to file (buffer-substring (point) bound))
	(goto-char bound)))
    (liece-send "NOTICE %s :\001FILE END %s : \001" to file)
    (kill-buffer (current-buffer))))

(provide 'liece-ctcp)

;;; liece-ctcp.el ends here
